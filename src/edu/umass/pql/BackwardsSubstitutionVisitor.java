/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql;
import edu.umass.pql.*;
import edu.umass.pql.il.meta.*;
import java.util.*;

/**
 * Takes a program representation that has read and write flags set properly and replaces all unused writes by wildcard writes.
 *
 */
public abstract class BackwardsSubstitutionVisitor extends JoinVisitor
{

	public abstract int subst(int var);
	public int substJoin(Join join, int var) { return subst(var); }
	public int substReductor(Reductor red, int var) { return subst(var); }

	private void adjust_var(Join bit, int index)
	{
		int var = bit.getArg(index);
		if (Env.isWildcard(var))
			return;
		int newvar = substJoin(bit, var);
		bit.setArg(index, newvar);
	}

	private void adjust_var_red(Reductor bit, int index)
	{
		int var = bit.getArg(index);
		if (var == Reductor.INPUT_FROM_INNER_REDUCTOR)
			return;
		if (Env.isWildcard(var))
			return;
		int newvar = substReductor(bit, var);
		bit.setArg(index, newvar);
	}

	private void adjust_var_reductor(boolean output_vars, Reductor bit)
	{
		if (bit == null)
			return;

		int start_offset;
		int stop_offset;
		if (output_vars) {
			start_offset = bit.getArgsNr() - 1;
			stop_offset = bit.getReadArgsNr();
		} else {
			start_offset = bit.getReadArgsNr();
			stop_offset = 0;
		}
		for (int i = start_offset; i >= stop_offset; i--)
			adjust_var_red(bit, i);

		adjust_var_reductor(output_vars, bit.getInnerReductor());
	}

	@Override
	public void
	visitDefault(Join it)
	{
		for (int i = it.getArgsNr() - 1; i >= 0; i--) {
			adjust_var(it, i);
		}
	}

	@Override
	public void
	visitSelectPath(SelectPath sp)
	{
		// no access path selection done on these, so we can't do anything interesting here
	}

	@Override
	public void
	visitReduction(Reduction red)
	{
		for (Reductor r : red.getReductors())
			adjust_var_reductor(false, r);

		red.getComponent(0).accept(this);

		for (Reductor r : red.getReductors())
			adjust_var_reductor(true, r);
	}

	@Override
	public void
	visitControlStructure(ControlStructure b)
	{
		visitDefault(b);
		for (int i = b.getComponentsNr() - 1; i >= 0; i--)
			b.getComponent(i).accept(this);
	}
}
