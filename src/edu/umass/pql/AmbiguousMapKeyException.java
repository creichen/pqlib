/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql;

public class AmbiguousMapKeyException extends RuntimeException
{
	public static final long serialVersionUID = 0xf0071005300002l;

	private Object key;

	public AmbiguousMapKeyException(Object key)
	{
		super("Ambiguous key: " + key);
		this.key = key;
	}

	public Object
	getKey()
	{
		return key;
	}

	public boolean
	equals(Object o)
	{
		if (!(o instanceof AmbiguousMapKeyException))
			return false;
		return ((AmbiguousMapKeyException) o).getKey().equals(this.getKey());
	}
}