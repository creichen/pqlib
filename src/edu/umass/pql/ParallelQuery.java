/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;

/**
 * This is a top-level parallelising evaluator, for queries that start with
 * a parallelisable toplevel reduction.  Its parallelism is more optimised
 * than that of OldParallelQuery, but it doesn't support non-`result' merging
 * (which is arguably an obsolete feature anyway, since even with multiple
 * results we can just put a result array into `result').
 *
 * By `result' I'm referring to the special-purpose field in CustomDecoratorJoin.
 */
public final class ParallelQuery
{
	static long all_init_times;
	static long all_eval_times;
	static long all_merge_times;
	static long max_init_times;
	static long max_eval_times;
	static long max_merge_times;

	static boolean SYNC_WRITE = getSynchronisedWrite();
	static int MERGE_THREAD_LIMIT = getMergeThreadLimit();
	//static ExecutorService thread_pool = Executors.newFixedThreadPool(Env.THREADS_NR);
	static CyclicBarrier[] barriers = init_barriers(Env.THREADS_NR);

	static CyclicBarrier[]
	init_barriers(int nr)
	{
		final CyclicBarrier[] retval = new CyclicBarrier[nr];
		for (int i = 0; i < nr; i++)
			retval[i] = new CyclicBarrier(2);
		return retval;
	}

	public static int
	getMergeThreadLimit()
	{
		Map<String, String> env = System.getenv();
		final String limit_str = env.get("PQL_MERGE_THREAD_LIMIT");
		if (limit_str == null)
			return -1;
		else
			return Integer.decode(limit_str);
	}

	public static boolean
	getSynchronisedWrite()
	{
		Map<String, String> env = System.getenv();
		final String str = env.get("PQL_SYNC_WRITE");
		if (str == null)
			return false;
		else
			return str.equalsIgnoreCase("true") || str.equals("1") || str.equals("yes") || str.equals("on");
	}

	public static void
	writeAndResetTimings(String output_file)
	{
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(output_file));
			out.write(all_init_times + "\t" + all_eval_times + "\t" + all_merge_times + "\tall");
			out.newLine();
			out.write(max_init_times + "\t" + max_eval_times + "\t" + max_merge_times + "\tMAX");
			out.newLine();
			out.close();

			clearTimings();
		}
		catch (IOException e)
		{
			System.out.println("Exception " + e);
		}
	}

	public static void
	clearTimings()
	{
		all_init_times = 0l;
		all_eval_times = 0l;
		all_merge_times = 0l;
		max_init_times = 0l;
		max_eval_times = 0l;
		max_merge_times = 0l;
	}


	public static synchronized // we can only run one query at a time, due to the shared thread pool and barriers
	Env
	query(Env env, Join join)
	{
		join.reset(env);

		if (join.hasRandomAccess()
		    && Env.THREADS_NR > 1
		    && PQLFactory.getParallelisationMode() != PQLFactory.ParallelisationMode.NONPARALLEL
		    && join instanceof CustomParallelDecoratorJoin
		    && ((CustomParallelDecoratorJoin)join).getComponentInternal(0) instanceof Reduction
		    && ((Reduction)((CustomParallelDecoratorJoin)join).getComponentInternal(0)).getReductors().length == 1) {

			int threads_nr;
			// if (PQLFactory.getParallelisationMode() == PQLFactory.ParallelisationMode.NONPARALLEL)
			// 	threads_nr = 1;
			// else
			threads_nr = Env.THREADS_NR;

			CustomParallelDecoratorJoin j = (CustomParallelDecoratorJoin) join;
			final Reductor reductor = ((Reduction)j.getComponentInternal(0)).getReductors()[0];

			ExecutorService thread_pool = Executors.newFixedThreadPool(Env.THREADS_NR);
			ParallelExecution pexec = new ParallelExecution(thread_pool, threads_nr, MERGE_THREAD_LIMIT,
									barriers,
									env, j, reductor);
			final Env result;
			if (SYNC_WRITE) {
				result = pexec.executeSynchronisedInParallel();
			} else {
				result = pexec.executeInParallel();
			}
			thread_pool.shutdown();

			return result;
		} else {
			join.reset(env);
			join.next(env);
			return env;

			//  --
			// Not sure what the below is all about:

			// CustomParallelDecoratorJoin j = (CustomParallelDecoratorJoin) join;
			// j.next(env);
			// final Reductor reductor = ((Reduction)j.getComponentInternal(0)).getReductors()[0];
			// env.setObject(reductor.getArg(reductor.getReadArgsNr()),
			// 	      j.result);

			// return env;
		}
	}

	public static class ParallelExecution
	{
		public static final boolean DEBUG = false;

		ExecutorService thread_pool;
		int map_threads_nr;
		int reduce_threads_nr;

		Env env;
		CustomParallelDecoratorJoin join_template;
		Reductor reductor_template;

		Reductor[] reductors;
		Evaluator[] evaluators;
		CyclicBarrier[] barriers;

		int index_space_size;

		public ParallelExecution(ExecutorService thread_pool_, int map_threads_nr_, int reduce_threads_nr_,
					 CyclicBarrier[] barriers_,
					 Env env_, CustomParallelDecoratorJoin join_, Reductor reductor_)
		{
			this.env = env_;
			this.join_template = join_;
			this.join_template.resetForRandomAccess(env_);
			this.index_space_size = join_template.getFanout(env);

			this.barriers = barriers_;
			this.thread_pool = thread_pool_;
			this.map_threads_nr = Math.min(map_threads_nr_, this.index_space_size);

			if (reduce_threads_nr_ <= 0)
				reduce_threads_nr_ = map_threads_nr_;

			this.reduce_threads_nr = Math.min(reduce_threads_nr_, this.map_threads_nr);
			this.reductor_template = reductor_;
			this.reductors = new Reductor[map_threads_nr];

			this.evaluators = new Evaluator[map_threads_nr];
			for (int i = 0; i < map_threads_nr; i++)
				evaluators[i] = new Evaluator(i);
		}

		public Env
		executeInParallel()
		{
			this.evaluateBody();

			this.env.setObject(this.reductors[0].getArg(this.reductors[0].getReadArgsNr()),
					   this.reductors[0].getAggregate());
			return this.env;
		}


		public void
		evaluateBody()
		{
			// Start
			for (int i = 0; i < map_threads_nr; i++)
				this.thread_pool.execute(this.evaluators[i]);
			
			// Wait for completion
			try {
				barriers[0].await();
			} catch (InterruptedException ex) {
				//return;
			} catch (BrokenBarrierException ex) {
				//return;
			}

			long longest_init = 0l;
			long longest_eval = 0l;
			long longest_merge = 0l;

			for (int i = 0; i < map_threads_nr; i++) {
				Evaluator e = this.evaluators[i];
				longest_init = Math.max(longest_init, e.getInitTime());
				longest_eval = Math.max(longest_eval, e.getEvalTime());
				longest_merge = Math.max(longest_merge, e.getMergeTime());
				all_init_times += e.getInitTime();
				all_eval_times += e.getEvalTime();
				all_merge_times += e.getMergeTime();
			}

			max_init_times += longest_init;
			max_eval_times += longest_eval;
			max_merge_times += longest_merge;
		}


		class Evaluator implements Runnable
		{
			int index;

			long start_time;
			long init_time;
			long eval_time;
			long merge_time;

			long getInitTime()
			{
				return init_time - start_time;
			}

			long getEvalTime()
			{
				return eval_time - init_time;
			}

			long getMergeTime()
			{
				return merge_time - eval_time;
			}

			public Evaluator(int i)
			{
				this.index = i;
			}

			public void
			run()
			{
				final CustomParallelDecoratorJoin join;
				final Reductor reductor;

				start_time = System.nanoTime();

				// Pre-initialisation
				reductor = reductor_template.copy();
				join = (CustomParallelDecoratorJoin) join_template.clone();

				// compute start and end
				final int index_space_chunk_size = index_space_size / map_threads_nr;
				final int index_space_bigger_chunks_nr = index_space_size - (index_space_chunk_size * map_threads_nr);

				final int bonus_size;
				final int bonus_offset;
				if (index < index_space_bigger_chunks_nr) {
					bonus_offset = index;
					bonus_size = 1;
				} else {
					bonus_offset = index_space_bigger_chunks_nr;
					bonus_size = 0;
				}
				final int start = index_space_chunk_size * index + bonus_offset;
				final int end = start + index_space_chunk_size + bonus_size - 1;

				init_time = System.nanoTime();

				if (DEBUG)
					System.err.println("#" + index + " here: running " + start + " -- " + end);

				// Evaluate
				reductor.setup(env);
				join.resetForRandomAccess(env);
				join.moveToRegion(start, end);
				join.next(env);
				reductor.setAggregate(join.result);

				eval_time = System.nanoTime();

				if (DEBUG)
					System.err.println(" #" + index + " here: I'm locally done: " + reductor.getAggregate());

				// Merge
				if (index < reduce_threads_nr) { // We get to do some merging!
					// First merge pass: linear merge:  if reduce threads are bounded,
					// merge all on our linear trajectory.
					final int max_possible_merge = map_threads_nr / reduce_threads_nr;
					for (int i = 1; i <= max_possible_merge; i++)
						tryMerge(reductor, index + (i * reduce_threads_nr));

					// Second merge pass: log merge.  
					// Consider the case of 8 threads.  We merge as follows:
					// #0: 4, 2, 1
					// #1: 5, 3
					// #2: 6
					// #3: 7
					// I.e., for d=4,2,1, each thread less than d merges in index+d.

					// compute delta as (reduce_threads_nr div 2), round up to nearest power of two
					int delta = 0x1;
					while ((delta << 1) < reduce_threads_nr)
						delta <<= 1;

					while (delta > 0 && index < delta) {
						if (index + delta < reduce_threads_nr) // only merge within the reduce-threads block
							tryMerge(reductor, index + delta);
						delta >>= 1;
					}
				}

				// Finish
				reductors[index] = reductor;
				merge_time = System.nanoTime();

				if (DEBUG)
					System.err.println("#" + index + " COMPLETE" + reductor.getAggregate());
				doAwait(index); // notify whoever merges in our result
			}

			final void
			tryMerge(Reductor reductor, int other_index)
			{
				if (other_index >= map_threads_nr)
					return;
				if (DEBUG)
					System.err.println(" #" + index + " here: trying to merge in #" + other_index);
				doAwait(other_index);
				reductor.absorbAggregate(reductors[other_index].getAggregate());
				if (DEBUG)
					System.err.println(" #" + index + " merge complete: "  + reductor.getAggregate());
			}

			final void
			doAwait(int i)
			{
				// Notify other threads of completion
				try {
					barriers[i].await();
				} catch (InterruptedException ex) {
					return;
				} catch (BrokenBarrierException ex) {
					return;
				}
			}
		}


		// ================================================================================
		// SYNC mode

		static CyclicBarrier para_barrier;

		public Env
		executeSynchronisedInParallel()
		{
			this.evaluateBodySynchronised();

			this.env.setObject(this.reductor_template.getArg(this.reductor_template.getReadArgsNr()),
					   this.join_template.result);
			return this.env;
		}

		public void
		evaluateBodySynchronised()
		{
			// Start
			this.para_barrier = new CyclicBarrier(map_threads_nr + 1);
			for (int i = 0; i < map_threads_nr; i++)
				this.thread_pool.execute(new SyncEvaluator(i));


			try {
				para_barrier.await();
			} catch (Exception __) {}
		}




		class SyncEvaluator implements Runnable
		{
			int index;

			public SyncEvaluator(int i)
			{
				this.index = i;
			}

			public void
			run()
			{
				final CustomParallelDecoratorJoin join;

				// Pre-initialisation
				join = (CustomParallelDecoratorJoin) join_template.clone();

				// compute start and end
				final int index_space_chunk_size = index_space_size / map_threads_nr;
				final int index_space_bigger_chunks_nr = index_space_size - (index_space_chunk_size * map_threads_nr);

				final int bonus_size;
				final int bonus_offset;
				if (index < index_space_bigger_chunks_nr) {
					bonus_offset = index;
					bonus_size = 1;
				} else {
					bonus_offset = index_space_bigger_chunks_nr;
					bonus_size = 0;
				}
				final int start = index_space_chunk_size * index + bonus_offset;
				final int end = start + index_space_chunk_size + bonus_size - 1;

				if (DEBUG)
					System.err.println("#" + index + " here: running " + start + " -- " + end);

				// Evaluate
				join.resetForRandomAccess(env);
				join.result = join_template.result;
				join.moveToRegion(start, end);
				join.synchronizedNext(env);

				if (DEBUG)
					System.err.println(" #" + index + " here: I'm locally done: " + join_template.result);

				try {
					para_barrier.await();
				} catch (Exception __) {}
			}
		}
	}
}
