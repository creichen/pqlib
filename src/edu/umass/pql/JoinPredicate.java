/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql;

/**
 * Test whether a given join is contained in a given set
 */
public abstract class JoinPredicate
{
	public abstract boolean contains(Join j);

	public static final JoinPredicate ANY  = new JoinPredicate() { public boolean contains(Join j) { return true; } };
	public static final JoinPredicate NONE = new JoinPredicate() { public boolean contains(Join j) { return false; } };

	public static final JoinPredicate
	rangeInference(final VarSet quantified_vars)
	{
		return new JoinPredicate()
		{
			public boolean
			contains(Join j)
			{
				final int offset = j.getRangeInferenceParameter();
				return (offset >= 0
					&& quantified_vars.contains(j.getArg(offset)));
			}
		};
	}
}
