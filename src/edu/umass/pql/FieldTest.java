/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import org.junit.*;
import static org.junit.Assert.*;

public class FieldTest extends TestBase
{
	A a = new A();
        
	@Test
	public void
	testIntAccess()
	{
		a.i = 1;
		env.setInt(i0w, 0);
		env.setInt(i1w, 0);

		env.setObject(o0w, a);
		env.setObject(o1w, null);

		checkTrue(PQLFactory.FIELD(A.class, "i", o0w, i0w));
		checkFalse(PQLFactory.FIELD(A.class, "i", o0w, i1r));
		assertEquals(1, env.getInt(i0w));
		assertEquals(0, env.getInt(i1w));

		checkFalse(PQLFactory.FIELD(A.class, "i", o1w, i2w));
	}


	@Test
	public void
	testLongAccess()
	{
		a.l = 1;
		env.setLong(l0w, 0l);
		env.setLong(l1w, 0l);

		env.setObject(o0w, a);
		env.setObject(o1w, null);

		checkTrue(PQLFactory.FIELD(A.class, "l", o0w, l0w));
		checkFalse(PQLFactory.FIELD(A.class, "l", o0w, l1r));
		assertEquals(1l, env.getLong(l0w));
		assertEquals(0l, env.getLong(l1w));

		checkFalse(PQLFactory.FIELD(A.class, "l", o1w, l2w));
	}


	@Test
	public void
	testDoubleAccess()
	{
		a.d = 1.0;
		env.setDouble(i0w, 0.0);
		env.setDouble(i1w, 0.0);

		env.setObject(o0w, a);
		env.setObject(o1w, null);

		checkTrue(PQLFactory.FIELD(A.class, "d", o0w, d0w));
		checkFalse(PQLFactory.FIELD(A.class, "d", o0w, d1r));
		assertEquals(1.0, env.getDouble(d0w), 0.0);
		assertEquals(0.0, env.getDouble(d1w), 0.0);

		checkFalse(PQLFactory.FIELD(A.class, "d", o1w, d2w));
	}


	@Test
	public void
	testObject()
	{
		String s = "foo";
		a.o = s;

		env.setObject(o0w, a);
		env.setObject(o1w, null);
		env.setObject(o2w, s);

		checkFalse(PQLFactory.FIELD(A.class, "o", o0w, o0r));
		assertSame(a, env.getObject(o0r));
		checkFalse(PQLFactory.FIELD(A.class, "o", o0w, o1r));
		assertNull(env.getObject(o1r));
		checkTrue(PQLFactory.FIELD(A.class, "o", o0w, o2r));
		assertSame(s, env.getObject(o2r));

		checkFalse(PQLFactory.FIELD(A.class, "o", o1w, o2w));

		checkTrue(PQLFactory.FIELD(A.class, "o", o0w, o1w));
		assertSame(s, env.getObject(o1r));
	}

	public static final class A
	{
		public int i;
		public long l;
		public double d;
		public Object o;
	}
}