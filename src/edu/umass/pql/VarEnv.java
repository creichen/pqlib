/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql;
import java.util.*;

public class VarEnv<T>
{
	private T default_value;
	protected Map<Integer, T> map;

	public VarEnv(T default_value)
	{
		this.default_value = default_value;
		this.map = new HashMap<Integer, T>();
	}

	public T
	lookup(int key)
	{
		T v = this.map.get(Env.writeVar(key));
		if (v == null)
			return this.default_value;
		else
			return v; 
	}

	public void
	insert(int key, T value)
	{
		if (!Env.isWildcard(key))
			this.map.put(Env.writeVar(key), value);
	}

	public VarEnv<T>
	copy()
	{
		final VarEnv<T> retval = new VarEnv<T>(this.default_value);
		retval.map.putAll(this.map);
		return retval;
	}
}