/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql;

import java.util.*;
import org.junit.*;
import static org.junit.Assert.*;
import static edu.umass.pql.PQLFactory.*;

public class BlockTest extends TestBase
{

	final Set<Integer> set = make_set(3);
	final Set<Integer> set2 = make_set(7);

	public static final Set<Integer>
	make_set(int size)
	{
		Set<Integer> s = new HashSet<Integer>();
		for (int i = 0; i < size; i++)
			s.add(i);
		return s;
	}

	// --------------------------------------------------------------------------------

	@Test
	public void
	testCBlockOne()
	{
		setInts(0, 0, 0);
		ControlStructure b = ConjunctiveBlock(new Join[] {
				PQLFactory.LT_Int(i0r, i1r)
			});
		testCount(0, b);
		setInts(0, 1, 0);
		testCount(1, b);
	}

	@Test
	public void
	testCBlockTwo()
	{
		Set<Integer> set = new HashSet<Integer>();
		setObjects(set, null, null);
		setInts(0, 10, 0);
		set.add(2);
		set.add(3);
		set.add(12);
		set.add(13);

		ControlStructure b = ConjunctiveBlock(new Join[] {
				PQLFactory.CONTAINS(o0r, i0w),
				PQLFactory.LT_Int(i0r, i1r)
			});

		testCount(2, b);
	}


	@Test
	public void
	testCBlockAPI()
	{
		ControlStructure b = PQLFactory.ConjunctiveBlock();
		assertEquals(0, b.getArgsNr());
		assertEquals(0, b.getReadArgsNr());
	}

	@Test
	public void
	testCBlockZero()
	{
		ControlStructure b = PQLFactory.ConjunctiveBlock();
		testCount(1, b);
	}


	// --------------------------------------------------------------------------------


	@Test
	public void
	testEmptyDBlock()
	{
		testCount(0, DisjunctiveBlock());
	}


	@Test
	public void
	testOneDBlock()
	{
		testCount(1, DisjunctiveBlock(TRUE));
	}


	@Test
	public void
	testOneBiggerDBlock()
	{
		setObjects(set, null, null);
		testCount(set.size(), DisjunctiveBlock(CONTAINS(o0r, i1w)));
	}


	@Test
	public void
	testTwoElementsDBlock()
	{
		setObjects(set, set2, null);
		testCount(set.size() + set2.size(),
			  DisjunctiveBlock(
					   CONTAINS(o0r, i1w), 
					   CONTAINS(o1r, i1w)));
	}


	@Test
	public void
	testThreeElementsDBlock()
	{
		setObjects(set, set2, null);

		Join j = DisjunctiveBlock(CONTAINS(o0r, i1w), 
					  FALSE,
					  CONTAINS(o1r, i1w));

		testCount(set.size() + set2.size(), j);

		Set<Integer> s = new HashSet<Integer>();
		Set<Integer> sr = new HashSet<Integer>();
		s.addAll(set);
		s.addAll(set2);
		sr.addAll(s);

		j.reset(env);
		while (j.next(env)) {
			int v = env.getInt(i1w);
			assertTrue (s.contains(v));
			sr.remove(v);
		};
		assertTrue(sr.isEmpty());

		j = DisjunctiveBlock(CONTAINS(o0r, i1w), 
				     TRUE,
				     CONTAINS(o1r, i1w));

		testCount(set.size() + set2.size() + 1, j);
	}


	@Test
	public void
	testDBlockAPI()
	{
		ControlStructure b = PQLFactory.DisjunctiveBlock();
		assertEquals(0, b.getArgsNr());
		assertEquals(0, b.getReadArgsNr());
	}

}