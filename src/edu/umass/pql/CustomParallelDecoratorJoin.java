/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql;

/**
 * Custom linear-access decorator, for use with custom-generated nonparallel joins that provide an
 * optimised version of the semantics of the join the decorator wraps.
 */
public abstract class CustomParallelDecoratorJoin extends CustomDecoratorJoin
{
	protected int state = 0;
	protected int initial_state = -1;
	protected int range_first;
	protected int range_last;
	// the following two must be set in the `init' implementation:
	protected int range_pos;
	protected int range_stop;

	public
	CustomParallelDecoratorJoin(Join body)
	{
		super(body);
	}

	@Override
	public DecoratorJoin
	copyRecursively()
	{
		CustomParallelDecoratorJoin mj = (CustomParallelDecoratorJoin) super.copyRecursively();
		return mj;
	}

	@Override
	public void
	accept(JoinVisitor v)
	{
		v.visitCustomDecorator(this);
	}

	// --------------------------------------------------------------------------------
	// sequential access

	@Override
	public abstract boolean
	next(Env env);

	public abstract boolean
	synchronizedNext(Env env);

	@Override
	public void
	reset(Env env)
	{
		this.state = 0;
	}

	// --------------------------------------------------------------------------------
	// parallel access

	final
	@Override
	public boolean
	hasRandomAccess()
	{
		return true;
	}

	@Override
	final public void
	resetForRandomAccess(Env env)
	{
		this.reset(env);
		this.initial_state = this.state;
		this.range_first = this.range_pos;
		this.range_last = this.range_stop;
	}

	public final void
	moveToRegion(int first, int last)
	{
		this.range_pos = first + this.range_first;
		this.range_stop = last + this.range_first;
		this.state = this.initial_state; // quasi-reset
	}

	final
	@Override
	public double
	getSize(VarInfo var_info, Env env)
	{
		return getFanout(env);
	}

	@Override
	final public int
	getAtIndex(Env env, int offset)
	{
		if (next(env))
			return GETAT_ANY;
		else
			return GETAT_NONE;
	}

	@Override
	final public void
	moveToIndex(Env env, int index)
	{
		this.range_pos = this.range_first + index;
		this.range_stop = this.range_pos;
		this.state = this.initial_state; // quasi-reset
	}

	@Override
	final public int
	getFanout(Env env)
	{
		return range_last - range_first + 1;
	}
}