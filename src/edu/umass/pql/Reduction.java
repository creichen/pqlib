/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql;

public abstract class Reduction extends ControlStructure
{
	/**
	 * Provides the number of variables that the reductor reads from outside of its loop, if any.
	 *
	 * These variables are always at the beginning of the variable list.
	 *
	 * @return Number of read-variables that must be initialised for the `Reduce' that this reductor is contained in
	 */
	public abstract int
	getOuterReadArgsNr();

	public abstract void
	addReductor(Reductor __);

	public abstract Reductor[]
	getReductors();

	public abstract boolean
	isParallel();

	@Override
	public void
	addAllVariables(VarSet set)
	{
		for (Reductor reductor : this.getReductors())
			reductor.addAllVariables(set, false);

		for (int i = 0; i < this.getComponentsNr(); i++)
			this.getComponent(i).addAllVariables(set);
	}

	@Override
	public void
	addActualReads(VarSet set, VarSet exclusions)
	{
		for (Reductor r : getReductors())
			r.addActualReads(set, exclusions);

		if (exclusions == null)
			exclusions = new VarSet();

		for (int i = 0; i < this.getComponentsNr(); i++)
			this.getComponent(i).addActualReads(set, exclusions);

		for (int i = this.getReadArgsNr(); i < this.getArgsNr(); i++)
			if (Env.isWriteVar(this.getArg(i)) && !Env.isWildcard(this.getArg(i)))
				exclusions.insert(this.getArg(i));

	}

	@Override
	public void
	addReadDependencies(VarSet set, VarSet exclusions)
	{
		for (int i = 0; i < this.getOuterReadArgsNr(); i++) {
			final int v = this.getArg(i);
			if (exclusions == null || !exclusions.contains(v)) {
				set.insert(this.getArg(i));
			}
		}

		if (exclusions == null)
			exclusions = new VarSet();

		for (int i = 0; i < this.getComponentsNr(); i++)
			this.getComponent(i).addReadDependencies(set, exclusions);

		for (int i = this.getReadArgsNr(); i < this.getArgsNr(); i++)
			exclusions.insert(this.getArg(i));

	}
}
