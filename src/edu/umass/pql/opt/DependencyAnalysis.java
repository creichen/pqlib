/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.opt;
import edu.umass.pql.*;
import edu.umass.pql.il.*;
import edu.umass.pql.il.meta.*;
import java.util.*;

public class DependencyAnalysis
{
	public static Map<Integer, VarSet>
	dependencies(Join main_join)
	{
		final Map<Integer, VarSet> depmap = new HashMap<Integer, VarSet>();

		class DepAnalyser extends ForwardVisitor
		{
			private final void
			visit(PQLParameterized pp)
			{
				for (int i = pp.getReadArgsNr(); i < pp.getArgsNr(); i++)
					if (pp.isValidArg(i)) {
						final int var = Env.readVar(pp.getArg(i));
						VarSet vs = depmap.get(var);
						if (vs == null) {
							vs = new VarSet();
							depmap.put(var, vs);
						}
						for (int j = 0; j < pp.getReadArgsNr(); j++) {
							final int dep = Env.readVar(pp.getArg(j));
							VarSet other_vs = depmap.get(dep);
							vs.insert(dep);
							if (other_vs != null)
								vs.addAll(other_vs);
						}
					}
			}

			@Override
			public void
			visitNonBlock(Join join)
			{
				visit(join);
			}

			@Override
			public void
			visitControlStructureBefore(ControlStructure join)
			{
			}

			@Override
			public void
			visitControlStructureAfter(ControlStructure join)
			{
				this.visitNonBlock(join);
			}

			@Override
			public void
			visitReductionBefore(Reduction red)
			{
			}

			private final void
			visitReductor(Reductor r)
			{
				if (r == null)
					return;

				visit(r);
				visitReductor(r.getInnerReductor());
			}

			@Override
			public void
			visitReductionAfter(Reduction red)
			{
				final Reductor[] reductors = red.getReductors();
				for (Reductor r : reductors)
					visitReductor(r);
			}
		};

		main_join.accept(new DepAnalyser());

		Iterator<Map.Entry<Integer, VarSet>> it = depmap.entrySet().iterator();
		while (it.hasNext()) {
			final Map.Entry<Integer, VarSet> entry = it.next();
			if (entry.getValue().isEmpty())
				it.remove();
		}
		return depmap;
	}
}