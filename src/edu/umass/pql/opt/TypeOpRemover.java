/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.opt;

import edu.umass.pql.*;
import edu.umass.pql.il.*;
import edu.umass.pql.il.meta.*;
import java.util.*;

import org.junit.*;
import static edu.umass.pql.PQLFactory.*;

public class TypeOpRemover<T> extends JoinVisitor
{
	TypeSystem<T> type_system;
	Join result;

	public TypeOpRemover(TypeSystem<T> ts)
	{
		this.type_system = ts;
	}

	public Join
	run(Join src)
	{
		src.accept(this);
		return this.getResult();
	}

	public Join
	getResult()
	{
		if (this.result == null)
			return TRUE;
		else
			return this.result;
	}

	private void
	preserveOnType(Join j, T checked_type)
	{
		T actual_type = this.type_system.getType(j.getArg(0));
		if (actual_type != null && checked_type != null
		    && Env.isReadVar(j.getArg(0))
		    && this.type_system.isSubtypeOf(actual_type, checked_type)) {
			this.result = null;
		} else
			this.result = j;
	}

	@Override
	public void
	visitJavaType(Type.JAVA_TYPE jty)
	{
		preserveOnType(jty, type_system.getTypeForJAVA_TYPE(jty.getType()));
	}

	@Override
	public void
	visitTypeChar(Join j)
	{
		preserveOnType(j, type_system.getCharType());
	}

	@Override
	public void
	visitTypeShort(Join j)
	{
		preserveOnType(j, type_system.getShortType());
	}

	@Override
	public void
	visitTypeByte(Join j)
	{
		preserveOnType(j, type_system.getByteType());
	}

	@Override
	public void
	visitTypeBoolean(Join j)
	{
		preserveOnType(j, type_system.getBooleanType());
	}

	@Override
	public void
	visitTypeInt(Join j)
	{
		preserveOnType(j, type_system.getIntType());
	}

	@Override
	public void
	visitTypeLong(Join j)
	{
		preserveOnType(j, type_system.getLongType());
	}

	@Override
	public void
	visitDefault(Join j)
	{
		this.result = j;
	}

	@Override
	public void
	visitControlStructure(ControlStructure b)
	{
		for (int i = 0; i < b.getComponentsNr(); i++)
			b.setComponent(i, run(b.getComponent(i)));
		this.result = b;
	}

	@Override
	public void
	visitConjunctiveBlock(AbstractBlock.Conjunctive b)
	{
		ArrayList<Join> results = new ArrayList<Join>();

		for (int i = 0; i < b.getComponentsNr(); i++) {
			b.getComponent(i).accept(this);
			if (this.result != null)
				results.add(this.result);
		}

		if (results.size() == 0)
			this.result = null;
		else
			this.result = PQLFactory.Conjunction(results);
	}
}
