/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.opt;

import edu.umass.pql.*;
import edu.umass.pql.il.*;
import edu.umass.pql.il.reductor.*;
import edu.umass.pql.il.meta.*;
import java.util.*;

import org.junit.*;
import static org.junit.Assert.*;
import static edu.umass.pql.PQLFactory.*;

public class ReductionNester extends JoinVisitor
{
	private final static boolean DEBUG = false;

	public static Reduction
	tryToNest(Reduction reduction)
	{
		if (reduction.getComponentsNr() > 1)
			return reduction; // this optimisation is not applicable there

		for (Reductor outer_reductor : reduction.getReductors()) {
			int result_var;
			int result_reductor_index;

			if (outer_reductor instanceof GenericMapReductor) {
				result_reductor_index = outer_reductor.getInnerReductorIndex();

				result_var = outer_reductor.getArg(result_reductor_index);

				if (DEBUG)
					System.err.println("[ReductionNest] Eligible outer reductor " + outer_reductor + ", index " + result_reductor_index + ", var " + Env.showVar(result_var));

				if (result_var == Reductor.INPUT_FROM_INNER_REDUCTOR)
					continue; // can't do anything with this one
			} else
				continue;

			ReductionNester nester = new ReductionNester(Env.writeVar(result_var));
			reduction.getComponent(0).accept(nester);

			if (nester.isEligible()) {
				Reductor inner_reductor = nester.getInnerReductor();
				if (DEBUG)
					System.err.println("Eligible inner reductor: " + inner_reductor);
				outer_reductor.setArg(result_reductor_index, Reductor.INPUT_FROM_INNER_REDUCTOR);
				outer_reductor.setInnerReductor(inner_reductor);
				inner_reductor.setArg(inner_reductor.getArgsNr() - 1, Env.WILDCARD);
				reduction.setComponent(0, nester.getBody());
			}
		}
		return reduction;
	}


	int reductive_var;
	public ArrayList<Join> body = new ArrayList<Join>();

	public ReductionNester(int var)
	{
		this.reductive_var = var;
	}

	Reductor
	getInnerReductor()
	{
		return this.inner_reductor;
	}

	Join
	getBody()
	{
		return AbstractBlock.flattenBlock(Conjunction(body));
	}

	Reductor inner_reductor;

	boolean
	isEligible()
	{
		return inner_reductor != null;
	}

	VarSet vars = new VarSet();

	@Override
	public void visitDefault(Join j)
	{
		j.addAllVariables(vars);
		if (DEBUG)
			System.err.println(">> Join " + j + " mods varset to " + vars);
		if (vars.contains(this.reductive_var))
			inner_reductor = null; // not eligible due to dependency

		body.add(j);
	}

	@Override
	public void visitConjunctiveBlock(AbstractBlock.Conjunctive b)
	{
		for (int i = 0; i < b.getComponentsNr(); i++)
			b.getComponent(i).accept(this);
	}

	@Override
	public void visitReduction(Reduction red)
	{
		if (this.inner_reductor != null) {
			visitDefault(red);
			return;
		}

		if (red.getComponentsNr() == 1
		    && red.getReductors().length == 1) {
			final Reductor potential_inner_reductor = red.getReductors()[0];
			if (potential_inner_reductor.getArg(potential_inner_reductor.getArgsNr() - 1) == this.reductive_var) {
				if (DEBUG)
					System.err.println("Found inner match!");

				if (vars.contains(this.reductive_var)) {
					// failed-- depended on by prior code
					if (DEBUG)
						System.err.println("Earlier deps disable it, though: " + vars);
					visitDefault(red);
					return;
				}

				inner_reductor = potential_inner_reductor;
				this.body.add(red.getComponent(0));
				return;
			}
			else { if (DEBUG)
					System.err.println("No match for " + Env.showVar(this.reductive_var) + " in " + potential_inner_reductor);}
		} else {
			if (DEBUG)
				System.err.println("Ineligible reduction: red");
		}
		// otherwise:
		
		visitDefault(red);
	}
}