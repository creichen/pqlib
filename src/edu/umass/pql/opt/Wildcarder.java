/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.opt;

import edu.umass.pql.*;
import edu.umass.pql.il.*;
import edu.umass.pql.il.meta.*;
import java.util.*;

import org.junit.*;
import static edu.umass.pql.PQLFactory.*;

public class Wildcarder extends JoinVisitor
{
	VarSet wanted_variables;

	public Wildcarder(VarSet set)
	{
		this.wanted_variables = set;
	}

	boolean
	wanted(int var)
	{
		return this.wanted_variables.contains(var);
	}

	public static void
	wildcard(Join join, VarSet wanted_variables)
	{
		Wildcarder w = new Wildcarder(wanted_variables);
		join.accept(w);
	}

	public void
	weedOutUnnecessaryBindings(PQLParameterized pp)
	{
		for (int i = pp.getReadArgsNr(); i < pp.getArgsNr(); i++)
			if (pp.isValidArg(i) && !wanted(pp.getArg(i)))
				pp.setArg(i, Env.WILDCARD);
	}

	// not needed: we only wildcard-out top-level bindings
	//
	// public void
	// weedOutUnnecessaryBindingsInReductor(Reductor r)
	// {
	// 	if (r == null)
	// 		return;
	// 	weedOutUnnecessaryBindings(r);
	// 	weedOutUnnecessarybindingsInReductor(r.getInnerReductor());
	// }

	@Override
	public void
	visitDefault(Join j)
	{
		weedOutUnnecessaryBindings(j);
	}

	@Override
	public void
	visitAnyBlock(AbstractBlock b)
	{
		for (int i = 0; i < b.getComponentsNr(); i++)
			b.getComponent(i).accept(this);
	}

	@Override
	public void
	visitReduction(Reduction red)
	{
		for (Reductor r : red.getReductors())
			weedOutUnnecessaryBindings(r);
	}
}
