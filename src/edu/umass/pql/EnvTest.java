/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql;

import org.junit.*;
import static org.junit.Assert.*;

public class EnvTest extends TestBase
{
	@Test
	public void
	testIntTableWrite()
	{
		final int i0 = Env.encodeVar(TYPE_INT, 0);
		final int i1 = Env.encodeVar(TYPE_INT, 1);

		env.setInt(i0, 1);
		env.setInt(i1, 2);
		assertEquals(env.getInt(i1), 2);
		assertEquals(env.getInt(i0), 1);

		env.setLong(i0, 3l);
		assertEquals(env.getInt(i0), 3);

		env.setDouble(i0, 4.0);
		assertEquals(env.getInt(i0), 4);

		env.setObject(i0, new Integer(5));
		assertEquals(env.getInt(i0), 5);

		env.setObject(i0, new Long(6));
		assertEquals(env.getInt(i0), 6);

		env.setObject(i0, Boolean.TRUE);
		assertEquals(env.getInt(i0), 1);

		env.setObject(i0, Boolean.FALSE);
		assertEquals(env.getInt(i0), 0);

		env.setObject(i0, new Float(7.0f));
		assertEquals(env.getInt(i0), 7);

		try {
			env.setObject(i0, "one");
			fail();
		} catch (Exception __) {};

	}


	@Test
	public void
	testLongTableWrite()
	{
		final int i0 = Env.encodeVar(TYPE_LONG, 0);
		final int i1 = Env.encodeVar(TYPE_LONG, 1);

		env.setLong(i0, 1l);
		env.setLong(i1, 2l);
		assertEquals(env.getLong(i1), 2l);
		assertEquals(env.getLong(i0), 1l);

		env.setInt(i0, 3);
		assertEquals(env.getLong(i0), 3l);

		env.setDouble(i0, 4.0);
		assertEquals(env.getLong(i0), 4l);

		env.setObject(i0, new Integer(5));
		assertEquals(env.getLong(i0), 5l);

		env.setObject(i0, new Long(6));
		assertEquals(env.getLong(i0), 6l);

		env.setObject(i0, Boolean.TRUE);
		assertEquals(env.getLong(i0), 1l);

		env.setObject(i0, Boolean.FALSE);
		assertEquals(env.getLong(i0), 0l);

		env.setObject(i0, new Float(7.0f));
		assertEquals(env.getLong(i0), 7l);

		try {
			env.setObject(i0, "one");
			fail();
		} catch (Exception __) {};

	}


	@Test
	public void
	testDoubleTableWrite()
	{
		final int i0 = Env.encodeVar(TYPE_DOUBLE, 0);
		final int i1 = Env.encodeVar(TYPE_DOUBLE, 1);

		env.setDouble(i0, 1.0);
		env.setDouble(i1, 2.0);
		assertEquals(env.getDouble(i1), 2.0, 0.0);
		assertEquals(env.getDouble(i0), 1.0, 0.0);

		env.setInt(i0, 3);
		assertEquals(env.getDouble(i0), 3.0, 0.0);

		env.setLong(i0, 4l);
		assertEquals(env.getDouble(i0), 4.0, 0.0);

		env.setObject(i0, new Integer(5));
		assertEquals(env.getDouble(i0), 5.0, 0.0);

		env.setObject(i0, new Long(6));
		assertEquals(env.getDouble(i0), 6.0, 0.0);

		env.setObject(i0, Boolean.TRUE);
		assertEquals(env.getDouble(i0), 1.0, 0.0);

		env.setObject(i0, Boolean.FALSE);
		assertEquals(env.getDouble(i0), 0.0, 0.0);

		env.setObject(i0, new Float(7.0f));
		assertEquals(env.getDouble(i0), 7.0, 0.0);

		try {
			env.setObject(i0, "one");
			fail();
		} catch (Exception __) {};

	}


	@Test
	public void
	testObjectTableWrite()
	{
		final int i0 = Env.encodeVar(TYPE_OBJECT, 0);
		final int i1 = Env.encodeVar(TYPE_OBJECT, 1);
		final Object o0 = new Object();
		final Object o1 = "foo";

		env.setObject(i0, o0);
		env.setObject(i1, o1);
		assertSame(o1, env.getObject(i1));
		assertSame(o0, env.getObject(i0));

		env.setInt(i0, 3);
		assertEquals(new Integer(3), env.getObject(i0));

		env.setLong(i0, 4l);
		assertEquals(new Long(4l), env.getObject(i0));

		env.setDouble(i0, 5.0);
		assertEquals(new Double(5.0), env.getObject(i0));
	}




	@Test
	public void
	testIntReader()
	{
		Env e = new Env(INT_TABLE, new Object[] { new int[] { 1, 2 }});
		final int i1 = Env.encodeVar(TYPE_INT, 0);
		final int i2 = Env.encodeVar(TYPE_INT, 1);

		assertEquals(1, e.getInt(i1));
		assertEquals(2, e.getInt(i2));

		assertEquals(1l, e.getLong(i1));

		assertEquals(1.0, e.getDouble(i1), 0.0);

		assertEquals(new Integer(1), e.getObject(i1));

	}

	@Test
	public void
	testLongReader()
	{
		Env e = new Env(LONG_TABLE, new Object[] { new long[] { 1l, 2l }});
		final int i1 = Env.encodeVar(TYPE_LONG, 0);
		final int i2 = Env.encodeVar(TYPE_LONG, 1);

		assertEquals(1l, e.getLong(i1));
		assertEquals(2l, e.getLong(i2));

		assertEquals(1, e.getInt(i1));

		assertEquals(1.0, e.getDouble(i1), 0.0);

		assertEquals(new Long(1), e.getObject(i1));

	}

	@Test
	public void
	testDoubleReader()
	{
		Env e = new Env(DOUBLE_TABLE, new Object[] { new double[] { 1.0, 2.0 }});
		final int i1 = Env.encodeVar(TYPE_DOUBLE, 0);
		final int i2 = Env.encodeVar(TYPE_DOUBLE, 1);

		assertEquals(1.0, e.getDouble(i1), 0.0);
		assertEquals(2.0, e.getDouble(i2), 0.0);

		assertEquals(1l, e.getLong(i1));

		assertEquals(1, e.getInt(i1));

		assertEquals(new Double(1.0), e.getObject(i1));

	}

	@Test
	public void
	testObjectReader()
	{
		final Object o1 = new Object();
		final Object o2 = "foo";
		Env e = new Env(OBJECT_TABLE, new Object[] { new Object[] { o1, o2, new Integer(1), new Long(2), new Double(3.0), new int[3], new long[3], new double[3], new Object[3] }});
		final int i1 = Env.encodeVar(TYPE_OBJECT, 0);
		final int i2 = Env.encodeVar(TYPE_OBJECT, 1);
		final int i_int = Env.encodeVar(TYPE_OBJECT, 2);
		final int i_long = Env.encodeVar(TYPE_OBJECT, 3);
		final int i_double = Env.encodeVar(TYPE_OBJECT, 4);
		// final int i_int_a = Env.encodeVar(TYPE_OBJECT, 5);
		// final int i_long_a = Env.encodeVar(TYPE_OBJECT, 6);
		// final int i_double_a = Env.encodeVar(TYPE_OBJECT, 7);
		// final int i_object_a = Env.encodeVar(TYPE_OBJECT, 8);

		assertSame(o1, e.getObject(i1));
		assertSame(o2, e.getObject(i2));

		assertEquals(1, e.getInt(i_int));

		assertEquals(2l, e.getLong(i_long));

		assertEquals(3.0, e.getDouble(i_double), 0.0);

		try {
			e.getInt(i1);
			fail();
		} catch (Exception __) {};

		try {
			e.getLong(i1);
			fail();
		} catch (Exception __) {};

		try {
			e.getDouble(i1);
			fail();
		} catch (Exception __) {};

	}

	@Test
	public void
	testMultiInit()
	{
		Env e = new Env(INT_TABLE | DOUBLE_TABLE,
				new Object[] {
					new int[] { 1, 2 },
					new double[] { 3.0 }
				});
		final int ii1 = Env.encodeVar(TYPE_INT, 0);
		final int ii2 = Env.encodeVar(TYPE_INT, 1);
		final int id1 = Env.encodeVar(TYPE_DOUBLE, 0);

		assertEquals(1, e.getInt(ii1), 1);
		assertEquals(2, e.getInt(ii2), 2);
		assertEquals(3.0, e.getDouble(id1), 0.0);
	}



	@Test
	public void
	testUnifyInt()
	{
		final int etype = TYPE_INT;
		final int v0 = 1;
		final int v1 = 2;

		final int ri0 = Env.encodeReadVar(etype, 0);
		final int wi0 = Env.encodeWriteVar(etype, 0);

		assertTrue(env.unifyInt(wi0, v0));
		assertTrue(env.unifyInt(wi0, v1));
		assertFalse(env.unifyInt(ri0, v0));
		assertTrue(env.unifyInt(ri0, v1));
	}


	@Test
	public void
	testUnifyLong()
	{
		final int etype = TYPE_LONG;
		final long v0 = 1l;
		final long v1 = 2l;

		final int ri0 = Env.encodeReadVar(etype, 0);
		final int wi0 = Env.encodeWriteVar(etype, 0);

		assertTrue(env.unifyLong(wi0, v0));
		assertTrue(env.unifyLong(wi0, v1));
		assertFalse(env.unifyLong(ri0, v0));
		assertTrue(env.unifyLong(ri0, v1));
	}


	@Test
	public void
	testUnifyDouble()
	{
		final int etype = TYPE_DOUBLE;
		final double v0 = 1;
		final double v1 = 2;

		final int ri0 = Env.encodeReadVar(etype, 0);
		final int wi0 = Env.encodeWriteVar(etype, 0);

		assertTrue(env.unifyDouble(wi0, v0));
		assertTrue(env.unifyDouble(wi0, v1));
		assertFalse(env.unifyDouble(ri0, v0));
		assertTrue(env.unifyDouble(ri0, v1));
	}


	@Test
	public void
	testUnifyObject()
	{
		final int etype = TYPE_OBJECT;
		final Object v0 = new Object();
		final Object v1 = "foo";

		final int ri0 = Env.encodeReadVar(etype, 0);
		final int wi0 = Env.encodeWriteVar(etype, 0);

		assertTrue(env.unifyObject(wi0, v0));
		assertTrue(env.unifyObject(wi0, v1));
		assertFalse(env.unifyObject(ri0, v0));
		assertTrue(env.unifyObject(ri0, v1));
	}

}