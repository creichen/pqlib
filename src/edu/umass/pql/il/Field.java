/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql.il;
import edu.umass.bc.JoinFlagsVisitor;
import edu.umass.pql.*;

/**
 * Field subscription
 */
public class Field extends Predicate.Predicate2
{
	private java.lang.reflect.Field field;
	private Object pfield;

	@Override
	public int
	getReadArgsNr()
	{
		return 1;
	}

	public Object
	getField()
	{
		return this.pfield;
	}

	public
	Field(Object field, int obj, int result)
	{
		super(obj, result);
		this.pfield = field;
		if (pfield instanceof java.lang.reflect.Field)
			this.field = (java.lang.reflect.Field) pfield;
	}

	public boolean
	test(Env env)
	{
		final Object obj = env.getObject(this.v0);
		if (obj == null)
			return false;

		try {
			final boolean retval = env.unifyByType(Env.varType(this.v1), this.v1, field.get(obj));
			return retval;
		} catch (IllegalAccessException __) {}

		return false;
	}

	@Override
	public void
	accept(JoinVisitor visitor)
	{
		visitor.visitField(this);
	}

	@Override public String
	getConstructorName()
	{
		return null;
	}


	@Override public double	getAccessCost(VarInfo var_info, Env env) { return 1.0; }
	@Override public double	getSelectivity(VarInfo var_info, Env env) { return OBJECT_JOIN_SELECTIVITY; }
        @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
}