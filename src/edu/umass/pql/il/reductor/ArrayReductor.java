/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.il.reductor;
import java.util.BitSet;
import edu.umass.bc.ReductorFlagsVisitor;
import edu.umass.pql.*;

public abstract class ArrayReductor extends DefaultReductor
{
	protected BitSet occupied_indices = null; // positions in array that have been filled

	int max_index = 0;

	final int find_next_size(int min_size, int old_size)
	{
		if (old_size < 32768)
			old_size *= 2;
		else
			old_size += 32768;

		if (min_size > old_size)
			old_size = min_size + 1;
		return old_size;
	}

	/**
	 * Marks array index as occupied
	 *
	 * @param index The index to mark
	 * @return true iff the index had previously been filled already
	 */
	protected final boolean
	checkAndSetKnownIndex(int index) {
		boolean retval = this.occupied_indices.get(index);
		this.occupied_indices.set(index);
		return retval;
	}

	public ArrayReductor(int output, int key, int value) { super(output, new int[] { key, value }); }
	//public boolean succeed(Env env) { throw new UnsupportedOperationException(); };
	public void collect(Env __) { throw new UnsupportedOperationException(); };
	protected abstract void resetInternal();
	@Override public final void reset() {
		occupied_indices = new BitSet();
		resetInternal();
	}
	public java.lang.Object getAggregate() { throw new UnsupportedOperationException(); };
	public void setAggregateInternal(java.lang.Object new_aggregate) { throw new UnsupportedOperationException(); };
	@Override public final void accept(ReductorVisitor visitor) { visitor.visitArray(this); }

	public static final class Int extends ArrayReductor
	{
		int[] aggregator = new int[32];
		// void add(int k, int v)
		// {
		// 	if (k >= aggregator.length) {
		// 		int[] na = new int[next_size(k, aggregator.length)];
		// 		System.arraycopy(aggregator, 0, na, 0, max_index);
		// 		na = aggregator;
		// 	}
		// 	if (k <= max_index) {
		// 		if (aggregator[k] != 0 && aggregator[k] != v)
		// 			throw new AmbiguousMapKeyException(k);
		// 	} else
		// 		max_index = k;
		// 	aggregator[k] = v;
		// }

		public final void absorbAggregate(java.lang.Object other_aggregate) { throw new UnsupportedOperationException(); };
		protected void resetInternal() { throw new UnsupportedOperationException(); };
		public final java.lang.Object getAggregate() { return this; }
		public final void setAggregateInternal(java.lang.Object new_aggregate)
		{
			// Int other = (Int) new_aggregate;
			// this.aggregator = other.aggregator;
			// this.max_index = other.max_index;
		}

		public Int(int output, int key, int value) { super(output, key, value); }
		@Override public String getName() { return "INT_ARRAY"; };
                @Override public final int accept(ReductorFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class Float extends ArrayReductor
	{
		public Float(int output, int key, int value) { super(output, key, value); }
		@Override public String getName() { return "FLOAT_ARRAY"; };
		@Override public void absorbAggregate(java.lang.Object other_aggregate) { throw new UnsupportedOperationException(); };
		protected void resetInternal() { throw new UnsupportedOperationException(); };
                @Override public final int accept(ReductorFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class Boolean extends ArrayReductor
	{
		public Boolean(int output, int key, int value) { super(output, key, value); }
		@Override public String getName() { return "BOOLEAN_ARRAY"; };
		@Override public void absorbAggregate(java.lang.Object other_aggregate) { throw new UnsupportedOperationException(); };
		protected void resetInternal() { throw new UnsupportedOperationException(); };
                @Override public final int accept(ReductorFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class Char extends ArrayReductor
	{
		public Char(int output, int key, int value) { super(output, key, value); }
		@Override public String getName() { return "CHAR_ARRAY"; };
		@Override public void absorbAggregate(java.lang.Object other_aggregate) { throw new UnsupportedOperationException(); };
		protected void resetInternal() { throw new UnsupportedOperationException(); };
                @Override public final int accept(ReductorFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class Byte extends ArrayReductor
	{
		public Byte(int output, int key, int value) { super(output, key, value); }
		@Override public String getName() { return "BYTE_ARRAY"; };
		@Override public void absorbAggregate(java.lang.Object other_aggregate) { throw new UnsupportedOperationException(); };
		protected void resetInternal() { throw new UnsupportedOperationException(); };
                @Override public final int accept(ReductorFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class Short extends ArrayReductor
	{
		public Short(int output, int key, int value) { super(output, key, value); }
		@Override public String getName() { return "SHORT_ARRAY"; };
		@Override public void absorbAggregate(java.lang.Object other_aggregate) { throw new UnsupportedOperationException(); };
		protected void resetInternal() { throw new UnsupportedOperationException(); };
                @Override public final int accept(ReductorFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class Double extends ArrayReductor
	{
		public Double(int output, int key, int value) { super(output, key, value); }
		@Override public String getName() { return "DOUBLE_ARRAY"; };
		@Override public void absorbAggregate(java.lang.Object other_aggregate) { throw new UnsupportedOperationException(); };
		protected void resetInternal() { throw new UnsupportedOperationException(); };
                @Override public final int accept(ReductorFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class Long extends ArrayReductor
	{
		public Long(int output, int key, int value) { super(output, key, value); }
		@Override public String getName() { return "LONG_ARRAY"; };
		@Override public void absorbAggregate(java.lang.Object other_aggregate) { throw new UnsupportedOperationException(); };
		protected void resetInternal() { throw new UnsupportedOperationException(); };
                @Override public final int accept(ReductorFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class AnyObject extends ArrayReductor
	{
		private Object[] aggregate = null;

		private void
		setSize(int size)
		{
			if (aggregate.length == size) {
				return;
			}
			Object[] new_aggregate = new Object[size];
			int copy_size = size < aggregate.length ? size : aggregate.length;
			System.arraycopy(this.aggregate, 0, new_aggregate, 0, copy_size);
			this.aggregate = new_aggregate;
		}

		public AnyObject(int output, int key, int value) { super(output, key, value); }
		@Override public String getName() { return "OBJECT_ARRAY"; };

		private final void
		set(int index, Object obj) {
			if (checkAndSetKnownIndex(index)) {
				Object other_obj = this.aggregate[index];
				if (other_obj != obj
				    && (other_obj == null || !other_obj.equals(obj))) {
					System.err.println("i:" + index + ", old:" + other_obj + ", new:" + obj);
					throw new AmbiguousMapKeyException(index);
				}
				return;
			}
			if (index >= aggregate.length) {
				// resize as needed
				this.setSize(find_next_size(index + 1, this.aggregate.length));
			}
			this.aggregate[index] = obj;
		}

		@Override protected void
		resetInternal() {
			aggregate = new Object[1024];
		};

		@Override public void collect(Env env) {
			final int key = env.getInt(this.getInputVar(0));
			final Object value = env.getObject(this.getInputVar(1));
			this.set(key, value);
		};

		@Override public void
		absorbAggregate(java.lang.Object other_aggregate) {
			// fixme... yes, this is nontrivial
			throw new UnsupportedOperationException();
		};

                @Override public final int
		accept(ReductorFlagsVisitor visitor) {
			return visitor.visit(this);
		}

		@Override public Object
		getAggregate() {
			return this.aggregate;
		}

		@Override public void
		setAggregateInternal(java.lang.Object new_aggregate) {
			this.aggregate = (Object[]) aggregate;
		}
	}
}
