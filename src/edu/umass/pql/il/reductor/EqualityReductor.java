/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.il.reductor;
import edu.umass.pql.*;

public final class EqualityReductor extends DefaultReductor
{
	private Object result = null;

	public EqualityReductor(int output, int input) { super(output, new int[] { input }); }
	public final void reset() { this.result = null; };
	public final boolean succeed(Env env) { return env.unifyObject(this.getInputVar(0), result); }
	public final void collect(Env __) { throw new RuntimeException("Shouldn't be called"); }
	public final Object getAggregate() { return this.result; }
	public final void setAggregateInternal(Object new_aggregate) { this.result = new_aggregate; }

	public final void absorbAggregate(Object other_aggregate)
	{
		if (this.result == null)
			this.result = other_aggregate;
		else if (other_aggregate == null)
			return;
		else if (this.result != other_aggregate
			 && !this.result.equals(other_aggregate))
			throw new AmbiguousMapKeyException(null);
	}

	@Override public String getName() { return "EQ"; };

	public static final EqualityReductor DEFAULT = new EqualityReductor(0, 0); // to be used for equality merging

	@Override public final void accept(ReductorVisitor visitor) { visitor.visitEquality(this); }
}


