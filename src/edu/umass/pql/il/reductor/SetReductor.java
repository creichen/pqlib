/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.il.reductor;
import edu.umass.bc.ReductorFlagsVisitor;
import edu.umass.pql.container.*;
import edu.umass.pql.*;
import java.util.*;

/**
 * Constructs a set.
 */
public final class SetReductor extends DefaultReductor
{
	private Set<Object> collector;

	public SetReductor(int dest, int src)
	{
		super(dest, new int[] { src });
	}

	@Override
	protected void
	reset()
	{
		this.collector = new PSet<Object>();
	}

	@Override
	public void
	collect(Env env)
	{
		this.collector.add(env.getObject(this.getInputVar(0)));
	}

	@Override
	public SetReductor
	copy()
	{
		SetReductor retval = (SetReductor) super.copy();
		if (this.collector != null)
			retval.collector = new PSet<Object>(this.collector);
		return retval;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void
	setAggregateInternal(Object other)
	{
		this.collector = (Set<Object>) other;
	}

	@Override
	public Object
	getAggregate()
	{
		return this.collector;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void
	absorbAggregate(Object other)
	{
		this.collector.addAll((Set<Object>) other);
	}
	@Override public String getName() { return "SET"; };

	@Override public final void accept(ReductorVisitor visitor) { visitor.visitSet(this); }
        @Override public final int accept(ReductorFlagsVisitor visitor) { return visitor.visit(this); }
}
