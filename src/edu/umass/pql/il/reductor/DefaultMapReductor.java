/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.il.reductor;
import edu.umass.bc.ReductorFlagsVisitor;
import edu.umass.pql.container.*;
import edu.umass.pql.*;

public final class DefaultMapReductor extends GenericMapReductor<Object, Object>
{
	protected int default_reg;
	private Object default_value;

	public
	DefaultMapReductor(int dest, int deflt, int src_key, int src_value)
	{
		super(dest, src_key, src_value);
		this.default_reg = deflt;
	}

	@Override
	public int
	getInnerReductorIndex()
	{
		return 2;
	}

	@Override
	public void
	setupInternal(Env env)
	{
		default_value = env.getObject(this.default_reg);
	}

	@Override
	public void
	reset()
	{
		this.collector = new PDefaultMap<Object, Object>(this.default_value);
	}

	@Override
	protected void
	insertEntryIntoCollector(final Object key, final Object value)
	{
		if (value != this.default_value)
			this.collector.put(key, value);
	}

	@Override
	public int
	getReadArgsNr()
	{
		return super.getReadArgsNr() + 1;
	}

	@Override
	public int
	getOuterReadArgsNr()
	{
		return 1;
	}

	@Override
	public int
	getArgsNr()
	{
		return super.getArgsNr() + 1;
	}

	@Override
	public int
	getArgInternal(int i)
	{
		if (i == 0)
			return this.default_reg;
		else
			return super.getArgInternal(i - 1);
	}

	@Override
	public void
	setArgInternal(int i, int v)
	{
		if (i == 0)
			this.default_reg = v;
		else
			super.setArgInternal(i - 1, v);
	}

	public Reductor
	getInnerReductor()
	{
		return this.inner_reductor;
	}

	@Override public String getName() { return "DEFAULT_MAP"; };

	@Override public final void accept(ReductorVisitor visitor) { visitor.visitDefaultMap(this); }
        @Override public final int accept(ReductorFlagsVisitor visitor) { return visitor.visit(this); }
}
