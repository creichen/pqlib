/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql.il.reductor;
import edu.umass.pql.*;

public abstract class DefaultReductor extends Reductor
{
	private int[] inputs;
	protected Object result;

	@Override
	public int getArgsNr() { return 1 + this.inputs.length; }

	@Override
	public int getReadArgsNr() { return this.inputs.length; }

	@Override
	public DefaultReductor
	copy()
	{
		DefaultReductor retval = (DefaultReductor) super.copy();
		/*
		retval.inputs = new int[this.inputs.length];
		for (int i = 0; i < this.inputs.length; i++)
			retval.inputs[i] = this.inputs[i];
		*/
		return retval;
	}

	@Override
	public int
	getArgInternal(int i)
	{
		return this.inputs[i];
	}

	@Override
	public void
	setArgInternal(int i, int v)
	{
		this.inputs[i] = v;
	}

	protected final int getInputVar(int i) { return this.inputs[i]; };

	public DefaultReductor(int result_var, int[] inputs)
	{
		super(result_var);
		this.inputs = inputs;
		if (inputs == null)
			this.inputs = new int[0];
	}


	public boolean
	succeed(Env env)
	{
//		System.out.println("[RED] " + this + ": " + env.toString().replaceAll("\n", "  "));
		collect(env);
		return false;
	}

	// To implement:  resetInternal, mergeFrom, collect, copy, eval
	public abstract void
	collect(Env env);
}
