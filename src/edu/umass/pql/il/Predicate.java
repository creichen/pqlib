/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql.il;
import edu.umass.bc.RuntimeCreator;
import edu.umass.pql.*;

public abstract class Predicate extends Join
{
	private boolean done;

	/**
	 * Tests the predicate, returns true iff it hols
	 *
	 * @param env The environment to evaluate in
	 * @return Whether the predicate holds 
	 */
	public abstract boolean
	test(Env env);

	public Join
	copyRecursively()
	{
		return this.copy();
	}


	public final void
	reset(Env env)
	{
		done = false;
	};

	public final boolean
	next(Env env)
	{
		if (done)
			return false;
		done = true;               
                
		return test(env);
	}

	@Override
	public double
	getSize(VarInfo var_info, Env env)
	{
		return 1.0;
	}

	@Override
	public double
	getAccessCost(VarInfo var_info, Env env)
	{
		return 0.1;
	}

	@Override public String
	getConstructorName()
	{
		return this.getName();
	}


	public static abstract class Predicate2 extends Predicate {
		protected int v0, v1;

		/**
		 * Constructs a new comparison
		 */
		Predicate2(int v0, int v1)
		{
			this.v0 = v0;
			this.v1 = v1;
		}

		public int
		getArgsNr()
		{
			return 2;
		}

		public int
		getReadArgsNr()
		{
			return 2;
		}

		public void
		setArg(int i, int v)
		{
			if (i == 0)
				v0 = v;
			else if (i == 1)
				v1 = v;
			else throw new RuntimeException("Invalid index: " + i);
		}

		public int
		getArg(int i)
		{
			if (i == 0)
				return v0;
			else if (i == 1)
				return v1;
			else throw new RuntimeException("Invalid index: " + i);
		}
	}


	public static abstract class Predicate3 extends Predicate {
		protected int v0, v1, v2;

		Predicate3(int v0, int v1, int v2)
		{
			this.v0 = v0;
			this.v1 = v1;
			this.v2 = v2;
		}

		public int
		getArgsNr()
		{
			return 3;
		}

		public int
		getReadArgsNr()
		{
			return 3;
		}

		public void
		setArg(int i, int v)
		{
			if (i == 0)
				v0 = v;
			else if (i == 1)
				v1 = v;
			else if (i == 2)
				v2 = v;
			else throw new RuntimeException("Invalid index: " + i);
		}

		public int
		getArg(int i)
		{
			if (i == 0)
				return v0;
			else if (i == 1)
				return v1;
			else if (i == 2)
				return v2;
			else throw new RuntimeException("Invalid index: " + i);
		}
	}
}
