/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql.il;
import edu.umass.pql.*;
import edu.umass.pql.il.meta.*;
import java.util.*;

/**
 * Takes an initial program representation without proper assignment of read/write flags and introduces read/write
 * flags as necessary.
 *
 * Initial PQL IR programs only reference variable names, without information about whether the variables
 * should be read or written.  This analysis annotates the variables suitably.
 *
 */
public class ReadFlagSetterVisitor extends JoinVisitor
{
	private VarSet bound_vars; // variables that are known to be defined
	private VarSet modified_vars; // variables that have been altered during the use of this visitor
	private Set<Join> errors = null;

	private void
	addError(Join it)
	{
		if (this.errors == null)
			this.errors = new HashSet<Join>();
		this.errors.add(it);
	}

	/**
	 * Creates a new ReadFlagSetterVisitor.
	 *
	 * @param defined_variables: the set of variables that are known to have been set already.
	 */
	public ReadFlagSetterVisitor(VarSet varset)
	{
		this.bound_vars = varset;
		this.modified_vars = new VarSet();
	}

	public void
	reset(VarSet set)
	{
		this.bound_vars = set;
		this.modified_vars = new VarSet();
	}

	public Set<Join>
	getErrors()
	{
		return this.errors;
	}

	private void adjust_var_internal(final boolean must_read, final boolean must_write,
					 Join bit,
					 int index)
	{
		final int var = bit.getArg(index);
		if (must_read && must_write)
			addError(bit);
		else if (must_write) {
			bit.setArg(index, Env.writeVar(var));
			this.bound_vars.insert(var);
			this.modified_vars.insert(var);
		} else
			bit.setArg(index, Env.readVar(var));
	}

	private void
	adjust_var_reductor(boolean outer,
			    Join base,
			    Reductor reductor)
	{
		if (reductor == null) {
			addError(base);
			return;
		}
		final int index_start = outer? 0 : reductor.getOuterReadArgsNr();
		final int index_stop = outer? reductor.getOuterReadArgsNr() : reductor.getArgsNr();;
		for (int index = index_start; index < index_stop; index++) {
			final int var = reductor.getArg(index);

			if (var == Reductor.INPUT_FROM_INNER_REDUCTOR)
				adjust_var_reductor(outer, base, reductor.getInnerReductor());
			else {
				final boolean must_read = index < reductor.getReadArgsNr();
				final boolean must_write = !bound_vars.contains(reductor.getArg(index));

				if (must_read && must_write) {
					// System.err.println("In " + base);
					// System.err.println("  outer = " + outer);
					// System.err.println("  " + reductor);
					// System.err.println("  " + Env.showVar(var));
					// System.err.println("  bound: " + this.bound_vars);
					addError(base);
				} else if (must_write) {
					reductor.setArg(index, Env.writeVar(var));
					this.bound_vars.insert(var);
					this.modified_vars.insert(var);
				} else
					reductor.setArg(index, Env.readVar(var));
			}
		}
	}


	private void adjust_var(Join bit, int index)
	{
		final boolean must_read = index < bit.getReadArgsNr();
		final boolean must_write = !bound_vars.contains(bit.getArg(index));
		adjust_var_internal (must_read, must_write,
				     bit, index);
	}

	@Override
	public void
	visitDefault(Join it)
	{
		for (int i = 0; i < it.getArgsNr(); i++) {
			adjust_var(it, i);
		}
		//System.err.println("[+] D: " + it + "    :: " + this.bound_vars);
	}

	@Override
	public void
	visitSelectPath(SelectPath sp)
	{
		sp.setVarInfo(new VarInfo(this.bound_vars.copy()));
		for (Join join : sp.getComponents()) {
			// We assume that the `SelectPath' has been scheduled `properly'
			join.addAllVariables(this.bound_vars);
			//join.addAllVariables(this.modified_vars);
		}
	}

	@Override
	public void
	visitSchedule(Schedule schedule)
	{
		this.visitControlStructure(schedule);
		schedule.setVarInfo(new VarInfo(this.bound_vars.copy()));
	}

	@Override
	public void visitReduction(Reduction red)
	{
		final VarSet old_bound_vars = this.bound_vars.copy();
		for (Reductor r : red.getReductors())
			adjust_var_reductor(true, red, r);
		this.bound_vars = old_bound_vars;

		red.getComponent(0).accept(this);

		for (Reductor r : red.getReductors())
			adjust_var_reductor(false, red, r);
	}

	public void visitControlStructure(ControlStructure n)
	{
		final VarSet old_bound_vars = this.bound_vars;
		final VarSet outer_modified_vars = this.modified_vars;
		this.modified_vars = new VarSet();
		this.bound_vars = old_bound_vars.copy();
		n.getComponent(0).accept(this);
		this.bound_vars = old_bound_vars;

//		System.err.println("After visit:  newly-mod: " + this.modified_vars);
		// all variables modified in the body of the `not' may or may not be set, so mark them not set
		this.bound_vars.removeAll(this.modified_vars);
		this.modified_vars.addAll(outer_modified_vars);
//		System.err.println("After visit:  bound: " + this.bound_vars + ", mod: " + this.modified_vars);
		visitDefault(n);
	}

	@Override
	public void visitNot(Not n)
	{
		visitControlStructure(n);
	}


	@Override
	public void visitNoFail(NoFail n)
	{
		visitControlStructure(n);
	}

	@Override
	public void visitConjunctiveBlock(AbstractBlock.Conjunctive b)
	{
//		System.err.println("[Preblock] bound: " + this.bound_vars + ", mod: " + this.modified_vars);
		visitDefault(b);
		for (int i = 0; i < b.getComponentsNr(); i++)
			b.getComponent(i).accept(this);
	}

	@Override
	public void visitDisjunctiveBlock(AbstractBlock.Disjunctive b)
	{
		visitDefault(b);
		
		final VarSet main_bound_vars_result = this.bound_vars;
		final VarSet main_bound_vars = this.bound_vars.copy();
		final VarSet main_modified_vars = this.modified_vars;

		for (int i = 0; i < b.getComponentsNr(); i++) {
			this.bound_vars = main_bound_vars.copy();
			this.modified_vars = new VarSet();

			b.getComponent(i).accept(this);

			main_modified_vars.addAll(this.modified_vars);
			if (i == 0)
				main_bound_vars_result.addAll(this.bound_vars);
			else
				main_bound_vars_result.retainAll(this.bound_vars); // intersection
		}

		this.bound_vars = main_bound_vars_result;
		this.modified_vars = main_modified_vars;
	}

	// public void visitContains(Container.CONTAINS c) { visitDefault(c); }

	// public void visitLookup(Container.GeneralLookup lookup) { visitDefault(lookup); }
}
