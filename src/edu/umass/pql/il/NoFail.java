/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql.il;
import edu.umass.bc.JoinFlagsVisitor;
import edu.umass.bc.RuntimeCreator;
import edu.umass.pql.*;

/**
 * Logical short-circuit negation
 *
 * @author creichen
 */
public class NoFail extends ControlStructure
{
	private Join body;
	protected boolean done = false;

	@Override
	public int getArgsNr() { return 0; }
	@Override
	public int getReadArgsNr() { return 0;}
	@Override
	public void setArg(int i, int v)
	{
		throw new IllegalArgumentException("No arguments allowed in control structure");
	}
	@Override
	public int getArg(int i)
	{
		throw new IllegalArgumentException("No arguments allowed in control structure");
	};

	@Override
	public Join
	copyRecursively()
	{
		NoFail clone = (NoFail) this.copy();
		clone.body = body.copyRecursively();
		return clone;
	}

	@Override
	public int getComponentsNr() { return 1; }

	@Override
	public Join
	getComponentInternal(int __)
	{
		return body;
	}

	@Override
	public void
	setComponentInternal(int __, Join new_body)
	{
		body = new_body;
	}

	public NoFail(Join body)
	{
		this.body = body;
	}

	public void
	reset(Env env)
	{
		body.reset(env);
		this.done = false;
	}

	public void
	resetForRandomAccess(Env env)
	{
		body.resetForRandomAccess(env);
		this.done = false;
	}

	@SuppressWarnings("unused")
	protected boolean
	registerBodyNext(Env env, boolean had_next) { return true; }

	public boolean
	next(Env env)
	{
                if (RuntimeCreator.useRuntimeCreator) {
                    RuntimeCreator.init( this, env );
                    return RuntimeCreator.test(env);
                }
		if (this.done)
			return false;
		this.done = true;
		return registerBodyNext(env, this.body.next(env));
	}

	@Override
	public boolean
	hasRandomAccess()
	{
		return body.hasRandomAccess();
	}

	@Override
	public int
	getFanout(Env env)
	{
		return body.getFanout(env);
	}

	@Override
	public void
	moveToIndex(Env env, int index)
	{
		body.moveToIndex(env, index);
	}

	@Override
	public int
	getAtIndex(Env env, int index)
	{
		this.registerBodyNext(env, body.getAtIndex(env, index) != GETAT_NONE);
		return GETAT_ONE;
	}

	@Override
	public void
	accept(JoinVisitor visitor)
	{
		visitor.visitNoFail(this);
	}

	@Override
	public double
	getSize(VarInfo var_info, Env env)
	{
		return 1.0;
	}

	@Override
	public double
	getSelectivity(VarInfo var_info, Env env)
	{
		return 1.0;
	}

	@Override
	public double
	getAccessCost(VarInfo var_info, Env env)
	{
		return this.getComponentInternal(0).getAccessCost(var_info, env);
	}

	public static class Bool extends NoFail
	{
		private int result_var;

		public Bool(Join body, int result)
		{
			super(body);
			this.result_var = result;
		}

		@Override
		public int getArgsNr() { return 1; }

		@Override
		public void setArg(int i, int v)
		{
			if (i != 0)
				throw new IllegalArgumentException("No arguments allowed in control structure");
			this.result_var = v;
		}
		@Override
		public int getArg(int i)
		{
			if (i != 0)
				throw new IllegalArgumentException("No arguments allowed in control structure");
			return this.result_var;
		}

		@Override
		protected boolean
		registerBodyNext(Env env, boolean had_next)
		{
			return env.unifyInt(result_var,
					    had_next? 1 : 0);
		}

		@Override
		public void
		accept(JoinVisitor visitor)
		{
			visitor.visitBool(this);
		}
                
                @Override public int accept (JoinFlagsVisitor visitor) {return visitor.visit(this); }
	}
}
