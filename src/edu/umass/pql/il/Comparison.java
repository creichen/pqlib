/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql.il;
import edu.umass.bc.JoinFlagsVisitor;
import edu.umass.pql.*;

public abstract class Comparison
{
	private static abstract class EQ extends Predicate.Predicate2 {
		public int getReadArgsNr() { return 1; }

		public EQ (int v0, int v1) { super(v0, v1); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitEq(this); }
		@Override public boolean isSymmetric() { return true; }
	}

	public static final class EQ_Int extends EQ {
		public EQ_Int (int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyInt(v1, env.getInt(v0)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(1, 1.0, INT_JOIN_SELECTIVITY); }
                @Override public int accept(JoinFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class EQ_Long extends EQ {
		public EQ_Long (int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyLong(v1, env.getLong(v0)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(1, 1.0, LONG_JOIN_SELECTIVITY); }
                @Override public int accept(JoinFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class EQ_Double extends EQ {
		public EQ_Double (int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyDouble(v1, env.getDouble(v0)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(1, 1.0, OBJECT_JOIN_SELECTIVITY); }
                @Override public int accept(JoinFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class EQ_Object extends EQ {
		public EQ_Object (int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.unifyObject(v1, env.getObject(v0)); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(1, 1.0, OBJECT_JOIN_SELECTIVITY); }
                @Override public int accept(JoinFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class EQ_String extends EQ {
		public EQ_String (int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) {
			final Object o0 = env.getObject(v0);
			final Object o1 = env.getObject(v1);
			if (o0 == o1)
				return true;
			if (o0 == null)
				return false;
			return o0.equals(o1);
		}
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return getSelectivityFromReadFlag(1, 1.0, OBJECT_JOIN_SELECTIVITY); }
		@Override public void accept(JoinVisitor visitor) { visitor.visitEqEquals(this); }
                @Override public int accept(JoinFlagsVisitor visitor) { return visitor.visit(this); }
	}


	public static final class NEQ_Int extends Predicate.Predicate2 {
		public NEQ_Int (int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.getInt(v0) != env.getInt(v1); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return 1.0 - INT_JOIN_SELECTIVITY; }
		@Override public void accept(JoinVisitor visitor) { visitor.visitNeq(this); }
                @Override public int accept(JoinFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class NEQ_Long extends Predicate.Predicate2 {
		public NEQ_Long (int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.getLong(v0) != env.getLong(v1); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return 1.0 - LONG_JOIN_SELECTIVITY; }
		@Override public void accept(JoinVisitor visitor) { visitor.visitNeq(this); }
                @Override public int accept(JoinFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class NEQ_Double extends Predicate.Predicate2 {
		public NEQ_Double (int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.getDouble(v0) != env.getDouble(v1); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return 1.0 - FLOAT_JOIN_SELECTIVITY; }
		@Override public void accept(JoinVisitor visitor) { visitor.visitNeq(this); }
                @Override public int accept(JoinFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class NEQ_Object extends Predicate.Predicate2 {
		public NEQ_Object (int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.getObject(v0) != env.getObject(v1); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return 1.0 - OBJECT_JOIN_SELECTIVITY; }
		@Override public void accept(JoinVisitor visitor) { visitor.visitNeq(this); }
                @Override public int accept(JoinFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class NEQ_String extends Predicate.Predicate2 {
		public NEQ_String (int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) {
			final Object o0 = env.getObject(v0);
			final Object o1 = env.getObject(v1);
			if (o0 == o1)
				return false;
			if (o0 == null)
				return true;
			return !o0.equals(o1);
		}
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return 1.0 - OBJECT_JOIN_SELECTIVITY; }
		@Override public void accept(JoinVisitor visitor) { visitor.visitNeqEquals(this); }
                @Override public int accept(JoinFlagsVisitor visitor) { return visitor.visit(this); }
	}




	public static final class LT_Int extends Predicate.Predicate2 {
		public LT_Int (int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.getInt(v0) < env.getInt(v1); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return INEQUALITY_SELECTIVITY; }
		@Override public void accept(JoinVisitor visitor) { visitor.visitLt(this); }
                @Override public int accept(JoinFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class LTE_Int extends Predicate.Predicate2 {
		public LTE_Int (int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.getInt(v0) <= env.getInt(v1); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return INEQUALITY_SELECTIVITY; }
		@Override public void accept(JoinVisitor visitor) { visitor.visitLte(this); }
                @Override public int accept(JoinFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class LT_Long extends Predicate.Predicate2 {
		public LT_Long (int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.getLong(v0) < env.getLong(v1); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return INEQUALITY_SELECTIVITY; }
		@Override public void accept(JoinVisitor visitor) { visitor.visitLt(this); }
                @Override public int accept(JoinFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class LTE_Long extends Predicate.Predicate2 {
		public LTE_Long (int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.getLong(v0) <= env.getLong(v1); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return INEQUALITY_SELECTIVITY; }
		@Override public void accept(JoinVisitor visitor) { visitor.visitLte(this); }
                @Override public int accept(JoinFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class LT_Double extends Predicate.Predicate2 {
		public LT_Double (int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.getDouble(v0) < env.getDouble(v1); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return INEQUALITY_SELECTIVITY; }
		@Override public void accept(JoinVisitor visitor) { visitor.visitLt(this); }
                @Override public int accept(JoinFlagsVisitor visitor) { return visitor.visit(this); }
	}

	public static final class LTE_Double extends Predicate.Predicate2 {
		public LTE_Double (int v0, int v1) { super(v0, v1); }
		public boolean test(Env env) { return env.getDouble(v0) <= env.getDouble(v1); }
		@Override public double	getSelectivity(VarInfo var_info, Env env) { return INEQUALITY_SELECTIVITY; }
		@Override public void accept(JoinVisitor visitor) { visitor.visitLte(this); }
                @Override public int accept(JoinFlagsVisitor visitor) { return visitor.visit(this); }
	}
}
