/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/


package edu.umass.pql;
import edu.umass.pql.*;
import edu.umass.pql.il.meta.*;
import java.util.*;

/**
 * Takes a program representation that has read and write flags set properly and replaces all unused writes by wildcard writes.
 *
 */
public abstract class ForwardVisitor extends JoinVisitor
{
	public abstract void visitNonBlock(Join join);
	public abstract void visitControlStructureBefore(ControlStructure cs);
	public abstract void visitControlStructureAfter(ControlStructure cs);
	public void visitReductionBefore(Reduction red) { this.visitControlStructureBefore(red); }
	public void visitReductionAfter(Reduction red) { this.visitControlStructureAfter(red); }

	@Override
	public void
	visitDefault(Join join)
	{
		visitNonBlock(join);
	}

	@Override
	public void
	visitReduction(Reduction b)
	{
		visitReductionBefore(b);
		for (int i = 0; i < b.getComponentsNr(); i++)
			b.getComponent(i).accept(this);
		visitReductionAfter(b);
	}

	@Override
	public void
	visitControlStructure(ControlStructure b)
	{
		visitControlStructureBefore(b);
		for (int i = 0; i < b.getComponentsNr(); i++)
			b.getComponent(i).accept(this);
		visitControlStructureAfter(b);
	}
}
