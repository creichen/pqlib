/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package edu.umass.pql;

import java.io.*;
import java.util.*;

/**
 * This is a top-level parallelising evaluator, for queries that start with
 * a parallelisable toplevel reduction.  It's an alternative to (and copies a
 * lot of code from) ReductionImpl.
 *
 * Note that STRIPED mode is broken at this point.  Also, note that this code
 * does NOT get linked in right now; only ParallelQuery is used (though
 * ParallelQuery requires `result' based evaluation).
 *
 * FIXME: elim code duplication wrt ReductionImpl!
 */
public final class OldParallelQuery
{
	static long all_init_times;
	static long all_eval_times;
	static long all_merge_times;

	static int MERGE_THREAD_LIMIT = getMergeThreadLimit();
	static boolean PARALLEL_START = getDoParallelStart();
	static Thread[] thread_pool = init_thread_pool(Env.THREADS_NR);

	static Thread[]
	getThreadPool()
	{
		return thread_pool;
	}

	static private Thread[]
	init_thread_pool(int nr)
	{
		final Thread[] retval = new Thread[nr];
		for (int i = 0; i < nr; i++)
			retval[i] = new Thread();
		return retval;
	}

	public static int
	getMergeThreadLimit()
	{
		Map<String, String> env = System.getenv();
		final String limit_str = env.get("PQL_MERGE_THREAD_LIMIT");
		if (limit_str == null)
			return -1;
		else
			return Integer.decode(limit_str);
	}

	public static boolean
	getDoParallelStart()
	{
		Map<String, String> env = System.getenv();
		final String str = env.get("PQL_PARALLEL_START");
		return str != null && (str.equals("1") || str.equals("true") || str.equals("yes"));
	}


	public static void
	writeAndResetTimings(String output_file)
	{
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(output_file));
			out.write(all_init_times + "\t" + all_eval_times + "\t" + all_merge_times);
			out.newLine();
			out.close();

			clearTimings();
		}
		catch (IOException e)
		{
			System.out.println("Exception " + e);
		}
	}

	public static void
	clearTimings()
	{
		all_init_times = 0l;
		all_eval_times = 0l;
		all_merge_times = 0l;
	}


	public static
	Env
	query(Env env, Join join)
	{
		join.reset(env);

		if (join.hasRandomAccess()
		    //&& Env.THREADS_NR > 1
		    && PQLFactory.getParallelisationMode() != PQLFactory.ParallelisationMode.NONPARALLEL
		    && join instanceof CustomParallelDecoratorJoin
		    && ((CustomParallelDecoratorJoin)join).getComponentInternal(0) instanceof Reduction
		    && ((Reduction)((CustomParallelDecoratorJoin)join).getComponentInternal(0)).getReductors().length == 1) {
			CustomParallelDecoratorJoin j = (CustomParallelDecoratorJoin) join;
			final Reductor reductor = ((Reduction)j.getComponentInternal(0)).getReductors()[0];

			ParallelExecution pexec;
			//			if (PQLFactory.getParallelisationMode() == PQLFactory.ParallelisationMode.SEGMENTED)
			pexec = new ParallelExecutionSegmented(reductor, j, env);
			// else
			// 	pexec = new ParallelExecutionStriped(reductor, j, env);

			//System.err.println("<<PARALLEL EXECUTION: " + PQLFactory.getParallelisationMode() + ", " + Env.THREADS_NR + ">>");
			return pexec.executeInParallel();
		} else {
			join.next(env);
			return env;
		}
	}

	public static
	Env
	queryObject(Env env, Join join)
	{
		join.reset(env);
		CustomParallelDecoratorJoin j = (CustomParallelDecoratorJoin) join;
		final Reductor reductor = ((Reduction)j.getComponentInternal(0)).getReductors()[0];

		ParallelExecution pexec;
		//if (PQLFactory.getParallelisationMode() == PQLFactory.ParallelisationMode.SEGMENTED)
		pexec = new ParallelExecutionSegmented(reductor, j, env);
		// else
		// 	pexec = new ParallelExecutionStriped(reductor, j, env);

		pexec.do_not_clone_env = true;
		pexec.do_not_deep_clone_join = true;

		//System.err.println("<<PARALLEL EXECUTION: " + PQLFactory.getParallelisationMode() + ", " + Env.THREADS_NR + ">>");
		return pexec.executeInParallel();
	}

	public abstract static class ParallelExecution
	{
		Env env;
		CustomParallelDecoratorJoin body;
		Reductor[] reductors;
		public static final boolean DEBUG = false;
		protected int initialised_count;
		public boolean do_not_clone_env = false;
		public boolean do_not_deep_clone_join = false;

		public ParallelExecution(Reductor reductor, CustomParallelDecoratorJoin body, Env env)
		{
			this.env = env;
			this.body = body;
			this.reductors = new Reductor[] { reductor };
		}

		public Env
		executeInParallel()
		{
			this.reset();
			this.evaluateBody();

			return this.env;
		}

		public void
		reset()
		{
			this.initialised_count = this.reductors.length;

			if (!this.hasRunnerArray() || Env.THREADS_NR > this.getRunnersNr()) {
				this.allocateRunnerArray(Env.THREADS_NR);
				for (int i = 0; i < Env.THREADS_NR; i++) {
					final ReductionRunner runner = this.allocateRunner(i);
					runner.setMustCloneBodyAndEnv(i > 0);
					runner.setBody(this.reductors, this.body);

					runner.setThread(getThreadPool()[i]);
					runner.reset(env);
				}
			} else
				for (int i = 0; i < Env.THREADS_NR; i++) {
					final ReductionRunner runner = this.getRunner(i);
					runner.reset(env);
					runner.setMustCloneEnv(i > 0);
				}
		}

		// computes result, stores in `main' reductors

		protected final void
		doRun()
		{
			final long starttime = System.nanoTime();
			int size = Env.THREADS_NR;
			setThreadsToInitialise(size);
			// map
			for (int i = size - 1; i >= 0; i--) {
				this.getRunner(i).setEnv(env);
				if (!PARALLEL_START)
					this.getRunner(i).start();
			}
			if (PARALLEL_START)
				this.getRunner(0).start();
			final long setuptime = System.nanoTime();
			for (int i = 0; i < size; i++)
				this.getRunner(i).join();

			final long jointime = System.nanoTime();
			if (DEBUG) {
				for (int i = 0; i < size; i++)
					System.out.println("Thread #" + i + ":\t"
							   + this.getRunner(i).lastRuntime());
			}

			final long prereduce_time = System.nanoTime();
			// reduce

			if (MERGE_THREAD_LIMIT != -1) {
				int merge_threads = MERGE_THREAD_LIMIT;
				if (merge_threads < 1 || merge_threads > size)
					merge_threads = size;

				final int merge_block_entries = size / merge_threads;

				// Bresenham:
				final int missing_entries = size - (merge_threads * merge_block_entries);

				for (int i = 0; i < merge_threads; i++) {
					final int entries_nr = merge_block_entries + (i < missing_entries ? 1 : 0) - 1;
					ReductionRunner[] runners = new ReductionRunner[entries_nr];
					for (int k = 0; k < entries_nr; k++)
						runners[k] = this.getRunner(i + ((k + 1) * merge_threads));
					this.getRunner(i).mergeFromMulti(runners);
				}

				for (int i = 0; i < merge_threads; i++)
					this.getRunner(i).join();

				//System.err.println("Shrinking log-merge size from " + size + " to " + merge_threads);
				size = merge_threads;
			}

			for (int delta = 1; delta < size; delta <<= 1) {
				final int stride = delta << 1;

				for (int i = 0; i < size - delta; i += stride) {
					//System.err.println("D-merge(" + i + " <- " + (i + delta) + ")");
					this.getRunner(i).mergeFrom(this.getRunner(i + delta));
				} for (int i = 0; i < size - delta; i++)
					this.getRunner(i).join();
			}

			if (do_not_clone_env)
				this.env.setObject(this.getRunner(0).reductors[0].getArg(this.getRunner(0).reductors[0].getReadArgsNr()),
						   this.getRunner(0).reductors[0].getAggregate());

			final long reducetime = System.nanoTime();
			all_init_times += (setuptime - starttime);
			all_eval_times += (jointime - setuptime);
			all_merge_times += (reducetime - prereduce_time);
			if (DEBUG) {
				System.out.println("Setup : " + (setuptime - starttime));
				System.out.println("Join  : " + (jointime - setuptime));
				System.out.println("Reduce: " + (reducetime - prereduce_time));
				System.out.println("!TOTAL: " + (reducetime - starttime));
			}
		}

		protected abstract boolean
		hasRunnerArray();

		protected abstract int
		getRunnersNr();

		protected abstract void
		allocateRunnerArray(int size);

		protected abstract ReductionRunner
		allocateRunner(int offset);

		protected abstract ReductionRunner
		getRunner(int offset);

		public abstract boolean
		evaluateBody();


			private volatile static int initialisation_count;

			private static synchronized void
			decInitialisationCount()
			{
				--initialisation_count;
			}

			/**
			 * Set the numer of threads to run.  This count sets up a spin-lock so that all threads have initialised before starting computation.
			 *
			 */
			public static synchronized void
			setThreadsToInitialise(int count)
			{
				initialisation_count = count;
			}


		protected abstract class ReductionRunner implements Runnable
		{
			public Reductor[] reductors;
			protected Thread thread;

			ReductionRunner[] others;
			ReductionRunner other;
			static final int MODE_MAP = 0;// in this mode, we run `compute'.
			static final int MODE_MERGE = 1;// in this mode we merge with `other'.
			static final int MODE_MERGE_MULTI = 2;// in this mode we merge with `others'.
			int mode = MODE_MAP; 

			final int runner_number;
			protected Env env;
			public CustomParallelDecoratorJoin body;
			private boolean must_clone_body, must_clone_env;

			public ReductionRunner(int nr)
			{
				this.runner_number = nr;
			}

			public void
			setMustCloneBodyAndEnv(boolean must_clone)
			{
				this.must_clone_body = this.must_clone_env = must_clone;
			}


			public void
			setMustCloneEnv(boolean must_clone)
			{
				this.must_clone_env = must_clone;
			}

			public void
			setThread(Thread t)
			{
				this.thread = t;
			}

			public void
			reset(Env env)
			{
				this.mode = MODE_MAP;
				for (Reductor r : this.reductors)
					r.setup(env);
				this.body.reset(env);
			}

			public void
			mergeFrom(ReductionRunner other)
			{
				this.other = other;
				this.mode = MODE_MERGE;
				this.start();
			}

			public void
			mergeFromMulti(ReductionRunner[] others)
			{
				this.others = others;
				this.mode = MODE_MERGE_MULTI;
				this.start();
			}

			public void
			setBody(Reductor[] reductors, CustomParallelDecoratorJoin b)
			{
				this.reductors = reductors;
				this.body = b;
			}

			public void
			setEnv(Env env)
			{
				this.env = env;
			}

			protected void
			cloneEnv()
			{
				this.env = this.env.copy();
			}

			protected void
			cloneBody()
			{
				// synchronized (ReductionRunner.class) {
				final Reductor[] src_reductors = this.reductors;
				final CustomParallelDecoratorJoin b = this.body;

				final Reductor[] new_reductors = new Reductor[src_reductors.length];
				for (int i = 0; i < src_reductors.length; i++)
					new_reductors[i] = src_reductors[i].copy();
				this.reductors = new_reductors;

				if (do_not_deep_clone_join)
					this.body = (CustomParallelDecoratorJoin) b.clone();
				else
					this.body = (CustomParallelDecoratorJoin) b.copyRecursively();
				//}
			}

			public void
			setResult(boolean r)
			{
				this.result = r;
			}

			private boolean result;

			public boolean
			getResult()
			{
				return this.result;
			}

			abstract protected void
			compute();

			protected void
			waitToStart()
			{
				decInitialisationCount();
				while (initialisation_count > 0) {};
			}

			public void
			start()
			{
				this.thread = new Thread(this);

				this.thread.start();
			}

			public void
			join()
			{
				try {
					this.thread.join();
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
			}

			protected long last_start, last_finish, last_setup;

			public String
			lastRuntime()
			{
				return (last_finish - last_start)
					+ "\t= setup:\t"
					+ this.lastSetupTime()
					+ " + eval:\t"
					+ this.lastComputationTime();
			}

			public long
			lastSetupTime()
			{
				return last_setup - last_start;
			}

			public long
			lastComputationTime()
			{
				return last_finish - last_setup;
			}

			private void
			setReductors()
			{
				if (do_not_clone_env) {
					this.reductors[0].setAggregate(this.body.result);
				} else for (Reductor r : this.reductors) {
						r.setAggregate(env.getObject(r.getArg(r.getReadArgsNr())));
						//System.err.println("agg = " + env.getObject(r.getArg(r.getReadArgsNr())));
					}
			}

			//@Override // commented out for JDK 1.5 bug
			public void
			run()
			{
				this.last_start = System.nanoTime();
				switch (this.mode) {
				case MODE_MAP:
					if (PARALLEL_START) {
						final int child_nr = (this.runner_number * 2) + 1;
						if (child_nr < getRunnersNr()) {
							getRunner(child_nr).start();
							if (child_nr + 1 < getRunnersNr())
								getRunner(child_nr + 1).start();
						}
					}

					if (this.must_clone_env && !do_not_clone_env)
						this.cloneEnv();

					if (this.must_clone_body)
						this.cloneBody();

					this.must_clone_body = this.must_clone_env = false;

					waitToStart();
					this.last_setup = System.nanoTime();

					compute();
					setReductors();
					break;

				case MODE_MERGE:
					for (int i = 0; i < this.reductors.length; i++)
						this.reductors[i].absorbAggregate(this.other.reductors[i].getAggregate());
					break;
				case MODE_MERGE_MULTI:
					for (int k = 0; k < this.others.length; k++)
						for (int i = 0; i < this.reductors.length; i++)
							this.reductors[i].absorbAggregate(this.others[k].reductors[i].getAggregate());
					break;
				}
				this.last_finish = System.nanoTime();
			}

		}

		/*
		public boolean
		next(Env env)
		{
			if (this.initialised_count == 0)
				return false;

			final Join _binder = this.binder;
			final Join _evaluator = this.evaluator;
			final ArrayList<Reductor> _reductors = this.reductors;

			_binder.reset(env);

			throw new RuntimeException("Make me parallel!");
		}
		*/
	}

	public static final class ParallelExecutionSegmented extends ParallelExecution
	{
		public ParallelExecutionSegmented(Reductor reductor, CustomParallelDecoratorJoin body, Env env)
		{
			super(reductor, body, env);
		}

		@Override
		protected boolean
		hasRunnerArray()
		{
			return this.runners != null;
		}

		@Override
		protected void
		allocateRunnerArray(int size)
		{
			this.runners = new SegmentedReductionRunner[size];
		}

		@Override
		protected int
		getRunnersNr()
		{
			return this.runners.length;
		}

		@Override
		protected ReductionRunner
		allocateRunner(int offset)
		{
			return this.runners[offset] = new SegmentedReductionRunner(offset);
		}

		@Override
		protected ReductionRunner
		getRunner(int offset)
		{
			return this.runners[offset];
		}

		private SegmentedReductionRunner[] runners = null;

		// @Override
		// public Join
		// copyRecursively()
		// {
		// 	ParallelExecutionSegmented retval = (ParallelExecutionSegmented) super.copyRecursively();
		// 	retval.runners = null;
		// 	return retval;
		// }

		private final class SegmentedReductionRunner extends ReductionRunner
		{
			int start, end;

			public SegmentedReductionRunner(int nr)
			{
				super(nr);
			}

			public String
			toString()
			{
				return this.start + "--" + this.end;
			}

			public void
			setSpan(int start, int end)
			{
				this.start = start;
				this.end = end;
				if (DEBUG) {
					System.err.println("Running from: " + start + " to: " + end + ", in " + this.reductors[0]);
				}
			}


			// @Override
			// public String
			// lastRuntime()
			// {
			// 	return super.lastRuntime() +
			// 		"; reset:\t"
			// 		+ (this.last_finish - this.initial_reset_time);
			// }

			@Override
			public void
			compute()
			{
				final int _start = this.start;
				final int _end = this.end;
				final CustomParallelDecoratorJoin _body = this.body;
				final Env _env = this.env;
				final Reductor[] _reductors = this.reductors;

				//System.err.println("Checking range: " + _start + " -- " + _end);

				_body.resetForRandomAccess(_env);

				//outer:
				_body.moveToRegion(_start, _end);
				_body.next(env);
				//System.err.println("Done with range: " + _start + " -- " + _end);
			}
		}

		@Override
		public boolean
		evaluateBody()
		{
			if (this.initialised_count == 0) // all reductors are done
				return false;

			final CustomParallelDecoratorJoin _body = this.body;
			final Reductor[] _reductors = this.reductors;

			_body.resetForRandomAccess(env);

			final int size = _body.getFanout(env);

			final int block_size = size / this.runners.length;
			int longer_blocks = size - (block_size * this.runners.length);

			int offset = 0;
			int stride_size = block_size + 1;
			for (int i = 0; i < this.runners.length; i++) {
				if (longer_blocks-- == 0)
					stride_size--;

				this.runners[i].setSpan(offset, offset + stride_size - 1);
				offset += stride_size;
			}

			doRun();

			boolean retval = true;
			for (Reductor r : _reductors)
				if (/*r.isEnabled() &&*/ !r.eval(env)) { // r.eval() has inverse semantics
					// --this.initialised_count;
					retval = false;
				}
			this.initialised_count = 0; // we only support reductors that yield a single binding
			return retval;
		}
	}


	public static final class ParallelExecutionStriped extends ParallelExecution
	{
		public ParallelExecutionStriped(Reductor reductor, CustomParallelDecoratorJoin body, Env env)
		{
			super(reductor, body, env);
		}

		@Override
		protected boolean
		hasRunnerArray()
		{
			return this.runners != null;
		}

		@Override
		protected void
		allocateRunnerArray(int size)
		{
			this.runners = new StripedReductionRunner[size];
		}

		@Override
		protected int
		getRunnersNr()
		{
			return this.runners.length;
		}

		@Override
		protected ReductionRunner
		allocateRunner(int offset)
		{
			return this.runners[offset] = new StripedReductionRunner(offset);
		}

		@Override
		protected ReductionRunner
		getRunner(int offset)
		{
			return this.runners[offset];
		}

		private StripedReductionRunner[] runners = null;

		// @Override
		// public Join
		// copyRecursively()
		// {
		// 	ParallelExecutionStriped retval = (ParallelExecutionStriped) super.copyRecursively();
		// 	retval.runners = null;
		// 	return retval;
		// }

		private final class StripedReductionRunner extends ReductionRunner
		{
			int start, end, stride;

			public StripedReductionRunner(int nr)
			{
				super(nr);
			}


			public String
			toString()
			{
				return this.start + "--" + this.end + " stride " + this.stride;
			}

			public void
			setSpan(int start, int stride, int end)
			{
				this.start = start;
				this.stride = stride;
				this.end = end;
				if (DEBUG) {
					System.err.println("Running from: " + start + " to: " + end + " at stride: "
							   + stride + ", in " + this.reductors[0]);
				}
			}

			// @Override
			// public String
			// lastRuntime()
			// {
			// 	return super.lastRuntime() +
			// 		"; reset:\t"
			// 		+ (this.last_finish - this.initial_reset_time);
			// }

			@Override
			public void
			compute()
			{
				final int _start = this.start;
				final int _end = this.end;
				final int _stride = this.stride;
				final CustomParallelDecoratorJoin _body = this.body;
				final Env _env = this.env;
				final Reductor[] _reductors = this.reductors;

				//System.err.println("Checking range: " + _start + " -- " + _end);

				_body.resetForRandomAccess(_env);

				//outer:
				for (int i = _start; i <= _end; i += _stride) {
					_body.moveToRegion(i, i+1);
					//int run_loop =
					_body.next(_env);
					// if (run_loop != Join.GETAT_NONE)
					// 	do {
					// 		for (Reductor r : _reductors)
					// 			if (r.isEnabled()
					// 			    && r.succeed(env)) {
					// 				r.disable();
					// 				//retval |= r.eval(env);
					// 				//if (0 == --this.initialised_count) break outer;
					// 			}
					// 		if (run_loop != Join.GETAT_ONE)
					// 			run_loop = _body.getAtIndex(_env, i);
					// 	} while (run_loop == Join.GETAT_ANY);
				}
			}
		}

		@Override
		public boolean
		evaluateBody()
		{
			if (this.initialised_count == 0) // all reductors are done
				return false;

			final CustomParallelDecoratorJoin _body = this.body;
			//final Join _evaluator = this.evaluator;
			final Reductor[] _reductors = this.reductors;

			_body.resetForRandomAccess(env);

			final int size = _body.getFanout(env);

			final int stride_size = this.runners.length;

			for (int i = 0; i < this.runners.length; i++)
				this.runners[i].setSpan(i, stride_size, size - 1);

			doRun();

			boolean retval = true;
			for (Reductor r : _reductors)
				if (/*r.isEnabled() &&*/ !r.eval(env)) { // r.eval() has inverse semantics
					// --this.initialised_count;
					retval = false;
				}
			this.initialised_count = 0; // we only support reductors that yield a single binding
			return retval;
		}
	}
}