/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umass.bc;

import java.util.ArrayList;
import java.util.Stack;
import org.objectweb.asm.MethodVisitor;

/**
 *
 * @author Hilmar
 */
public class ReturnBehaviorStack implements BcFlags {
    
    protected Stack <Integer> stack;
    protected ArrayList <ReturnBehavior> list;
    protected Stack <Integer> stackSizeBefore;
    
    public ReturnBehaviorStack () {
        stack = new Stack <Integer>();
        stack.push(0);
        //stack.push( new ReturnBehavior() );*/
        list = new ArrayList <ReturnBehavior> ();
        stackSizeBefore = new Stack <Integer> ();
        list.add(new ReturnBehavior());
    }
    
    public void push (ReturnBehavior val) {
        list.add(val);
        stack.add(list.size()-1);
        //stack.push(val);        
        /*if (insertPtr < stack.size())
            stack.set(insertPtr, val);
        else
            stack.add(val);
        insertPtr--;*/
    }
    
    public void saveStack () {
        stackSizeBefore.push(stack.size());
    }
    
    public void restoreStack () {
        Integer stackSizeGoal = stackSizeBefore.pop();
        while (stack.size() > stackSizeGoal)
            stack.pop();
    }
    
    /*public void pop () throws Exception {
        if (stack.size() <= 1)
            throw new Exception("You are trying to pop the last ReturnBehavior of the stack (internal error).");
        stack
        //stack.pop();
        //stack.get(insertPtr-1);
        insertPtr --;
    }*/
    
    /*public int getSize() {
        return insertPtr;
    }*/
    
    public void insertReturnCode (boolean result, MethodVisitor mv) throws Exception {
        list.get(stack.lastElement()).insertReturnCode(result, mv, this);
    }
    
    /*public void insertReturnCode (boolean result, MethodVisitor mv, int index) throws Exception {
        //ReturnBehavior lastBehavior = stack.elementAt(index);
        //lastBehavior.insertReturnCode(result, mv, stack);
        System.out.println(index + " : " + insertPtr + " / " + stack.size());
        stack.get(insertPtr-1).insertReturnCode(result, mv, stack.get(index));
    }*/
    
}
