CP=".:../lib/asm-all-5.2.jar:../lib/antlr-4.2.2-complete.jar:../lib/junit-4.0.jar:../lib/GenLib.jar:../lib/graph/jfreechart-1.0.19.jar:../lib/graph/jcommon-1.0.23.jar:../lib/weka.jar"
JAVAC=javac -target 1.8 -source 1.8 -Xlint:deprecation -Xlint:unchecked -cp ${CP}
JAVA=java
SOURCES=`find . -name "*.java" | grep -v \\\#`
JAR=pqlib.jar

default: jar

all:
	(cd src; ${JAVAC} `find . -name "*.java" | grep -v \\\# | egrep -v 'benchmarks\/.*\/PQL.java'`)

clean:
	rm -f `find . -name "*.class"`

test: all
	(cd src; ${JAVA} -cp ${CP}  org.junit.runner.JUnitCore `find edu -name "*Test.java" | grep -v \\\# | sed 's/^\.\///' | sed 's/\.java//' | tr '/' '.'`)

benchmark: all bench/MiniBench.class
	${JAVA} bench.MiniBench

jar: all
	rm -f ${JAR}
	(cd src; jar cf ../${JAR} `find . -name "*.class"`)

