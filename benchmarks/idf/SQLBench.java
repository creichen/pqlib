package benchmarks.idf;
import benchmarks.Evaluator;

import benchmarks.webgraph.Generator;
import edu.umass.pql.container.PDefaultMap;
import benchmarks.SQL;
import java.sql.*;

import java.util.*;

public class SQLBench extends Evaluator
{
	public SQLBench(SQL sql)
	{
		this.sql = sql;
	}

	public SQL sql;

	PreparedStatement query;

	@Override
	public void
	init()
	{
		Generator.install_sql(sql);
	}

	@Override
	public void
	cleanup()
	{
		Generator.uninstall_sql(sql);
	}

	@Override
	public void
	compute()
	{
		try {
			final String docs = sql.tableName("docs");
			final String words = sql.tableName("words");
			final String links = sql.tableName("links");

			this.query = sql.prepareQuery("select word, COUNT(docid) from " + words + " group by word;");

			final ResultSet res = sql.runQuery(query);

			Map<Integer, Integer> results = new PDefaultMap<Integer, Integer>(0);
			while (res.next()) {
				results.put(res.getInt(1), res.getInt(2));
			}

			this.result = results;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String
	getName()
	{
		return this.sql.getDriverName();
	}
}
