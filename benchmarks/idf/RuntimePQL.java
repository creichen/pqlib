package benchmarks.idf;
import edu.umass.bc.RuntimeCreator;
import bench.RuntimeCreatorBench;
import benchmarks.Evaluator;
import benchmarks.webgraph.Webdoc;
import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.TableConstants;
import edu.umass.pql.VarConstants;
import edu.umass.pql.container.*;
import java.util.*;

public class RuntimePQL extends Evaluator implements TableConstants
{
    
        protected Env env;
        protected Join j;
        protected RuntimeCreatorBench rc;
    
	@Override
	public void
	init()
	{
            env = new Env (INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[] {
                        new int[14],
                        new long[5],
                        new double[5],
                        new Object[9]
                });
            
            env.v_int[0] = 0;
            env.v_object[1] = benchmarks.webgraph.Generator.documents;
            env.v_object[0] = new Webdoc(0);
            env.v_object[2] = new int [1];
            env.v_object[3] = new PDefaultMap();
        
            rc = new RuntimeCreatorBench();                
            j = rc.idf(env);            
            RuntimeCreator.clearTypeInformations();
            //RuntimeCreator.setTypeInformation(0);
            RuntimeCreator.setTypeInformation(1, env);
            RuntimeCreator.setTypeInformation(2, env);
            RuntimeCreator.setTypeInformation(3, env);
            RuntimeCreator.init(j, env);
            RuntimeCreator.alreadyInitialized = true;
        }
    
        public void
	compute()
	{
            rc.compute(j);            
            this.result = rc.getEnv().v_object[3];
	}

	public String
	getName()
	{
		return "runtime_pql";
	}
}
