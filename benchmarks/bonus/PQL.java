package benchmarks.bonus;
import benchmarks.Evaluator;
import static edu.umass.pql.Query;
import static edu.umass.pql.Query.sumDouble;
import java.util.*;

public class PQL extends Evaluator.PQLEvaluator
{
	public void
	compute()
	{
		Set<Employee> employees = Generator.employees;

		result = query(Map.get(employee) == double bonus):
		           employees.contains(employee)
				   && bonus ==
				       employee.dept.bonus_factor
				       * (reduce(sumDouble) v:
					  exists Bonus b: employee.bonusSet.contains(b)
					  && v == b.bonus_base);
	}
}
