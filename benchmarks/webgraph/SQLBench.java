package benchmarks.webgraph;
import benchmarks.Evaluator;
import edu.umass.pql.container.*;
import benchmarks.SQL;
import java.sql.*;

import java.util.*;

public class SQLBench extends Evaluator
{
	public SQLBench(SQL sql)
	{
		this.sql = sql;
	}

	public SQL sql;

	PreparedStatement query;

	@Override
	public void
	init()
	{
		Generator.install_sql(sql);
	}

	@Override
	public void
	cleanup()
	{
		Generator.uninstall_sql(sql);
	}

	@Override
	public void
	compute()
	{
		try {
			final String docs = sql.tableName("docs");
			final String words = sql.tableName("words");
			final String links = sql.tableName("links");
			this.query = sql.prepareQuery("select " + links + ".source from " + links + " join " + links + " as links2 on " + links + ".destination = links2.source and links2.destination = " + links + ".source;");

			final ResultSet res = sql.runQuery(query);

			PSet<Webdoc> results = new PSet<Webdoc>();
			while (res.next()) {
				results.add(Generator.documents_array[res.getInt(1)]);
			}

			this.result = results;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String
	getName()
	{
		return this.sql.getDriverName();
	}
}
