package benchmarks.webgraph;
import edu.umass.bc.RuntimeCreator;
import bench.RuntimeCreatorBench;
import benchmarks.Evaluator;
import edu.umass.pql.Env;
import edu.umass.pql.Join;
import edu.umass.pql.TableConstants;
import edu.umass.pql.VarConstants;
import edu.umass.pql.container.*;
import java.util.*;

public class RuntimePQL extends Evaluator implements TableConstants
{
    
        protected Env env;
        protected Join j;
        protected RuntimeCreatorBench rc;
    
	@Override
	public void
	init()
	{
            env = new Env (INT_TABLE | LONG_TABLE | DOUBLE_TABLE | OBJECT_TABLE,
                new Object[] {
                        new int[14],
                        new long[5],
                        new double[5],
                        new Object[9]
                });            
            
            rc = new RuntimeCreatorBench();                
            j = rc.webgraph(env);            
            RuntimeCreator.clearTypeInformations();
            //RuntimeCreator.setTypeInformation(0);
            RuntimeCreator.setTypeInformation(1, env);
            //RuntimeCreator.setTypeInformation(2);
            RuntimeCreator.setTypeInformation(3, env);
            //RuntimeCreator.setTypeInformation(4);
            RuntimeCreator.setTypeInformation(5, env);
            RuntimeCreator.setTypeInformation(6, env);
            RuntimeCreator.setTypeInformation(8, env);
            RuntimeCreator.init(j, env);
            RuntimeCreator.alreadyInitialized = true;
        }
    
        public void
	compute()
	{
            rc.compute(j);
            this.result = rc.getEnv().v_object[8];
	}

	public String
	getName()
	{
		return "runtime_pql";
	}
}
